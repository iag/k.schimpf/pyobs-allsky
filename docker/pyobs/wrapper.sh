#!/bin/bash

# Start the first process
poetry run pyobs config/filecache.yaml &

# Start the second process
poetry run pyobs config/camera.yaml &

# Wait for any process to exit
wait -n

# Exit with status of process that exited first
exit $?

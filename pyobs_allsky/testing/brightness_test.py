import asyncio
import logging
import os
from typing import Any

import numpy as np

from pyobs.interfaces import IAutonomous, ICamera
from pyobs.modules import Module

from pyobs_allsky.brightness_estimator.image_average_calculator import ImageAverageCalculator
from pyobs_allsky.image.filenname_generator import FilenameGenerator
from pyobs_allsky.image.fits_saver import FitsStrategy
from pyobs_allsky.imaging_iterator.camera_controller import CameraController


log = logging.getLogger(__name__)


class BrightnessTest(Module, IAutonomous):
    def __init__(self, camera: str,
                 start_exposure_time: float, stop_exposure_time: float, step_size: float,
                 download: str, mask: str,
                 *args, **kwargs):
        Module.__init__(self, *args, **kwargs)

        self._camera = camera
        self._saver = FitsStrategy(download)
        self._download = download
        mask = np.load(mask)
        self._average_calc = ImageAverageCalculator(mask)

        self._start_exposure_time = float(start_exposure_time)
        self._stop_exposure_time = float(stop_exposure_time)
        self._step_size = float(step_size)

        self.add_background_task(self._run_thread, False)

    async def _run_thread(self):
        camera = CameraController(await self.proxy(self._camera, ICamera), self.vfs)

        steps = np.arange(start=self._start_exposure_time, stop=self._stop_exposure_time, step=self._step_size)

        for n, exposure_time in enumerate(steps):

            log.info(f"No: {n+1}/{len(steps)}")

            await camera.apply_camera_settings(exposure_time, 1.0)
            image = await camera.take_image()

            image.header["HIERARCH NO"] = n

            self._saver.save(image, FilenameGenerator(image)())

            await asyncio.sleep(1.0)

    async def start(self, **kwargs: Any) -> None:
        pass

    async def stop(self, **kwargs: Any) -> None:
        pass

    async def is_running(self, **kwargs: Any) -> bool:
        return True

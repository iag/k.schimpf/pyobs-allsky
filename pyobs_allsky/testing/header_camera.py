from typing import Any

from astropy.io import fits
from astropy.time import Time

from numpy._typing import NDArray
from pyobs.utils.simulation import SimCamera


class HeaderCamera(SimCamera):
    def _create_header(self, exp_time: float, open_shutter: float, time: Time, data: NDArray[Any]) -> fits.Header:
        # create header
        hdr = fits.Header()
        #hdr["NAXIS1"] = data.shape[1]
        #hdr["NAXIS2"] = data.shape[0]

        hdr["NAXIS1"], hdr["NAXIS2"] = 2821, 2071

        # set values
        hdr["DATE-OBS"] = (time.isot, "Date and time of start of exposure")
        hdr["EXPTIME"] = (exp_time, "Exposure time [s]")

        hdr["LATITUDE"] = 51.559299
        hdr["LONGITUD"] = 9.945472
        hdr["HEIGHT"] = 201

        hdr["WCSAXES"] = 2
        hdr["CRPIX1"] = 1195.2545605796
        hdr["CRPIX2"] = 1031.8076958883
        hdr["PC1_1"] = -0.00060084027941286
        hdr["PC1_2"] = -0.052057192086615
        hdr["PC2_1"] = 0.054891854602827
        hdr["PC2_2"] = -0.00073949896926166
        hdr["CDELT1"] = 1.0
        hdr["CDELT2"] = 1.0
        hdr["CUNIT1"] = "deg"
        hdr["CUNIT2"] = "deg"
        hdr["CTYPE1"] = "RA---AIR-SIP"
        hdr["CTYPE2"] = "DEC--AIR-SIP"
        hdr["CRVAL1"] = 359.61108550652
        hdr["CRVAL2"] = 31.416129776264
        hdr["LONPOLE"] = 180.0
        hdr["LATPOLE"] = 31.416129776264
        hdr["MJDREF"] = 0.0
        hdr["RADESYS"] = "ICRS"

        hdr["A_ORDER"] = 3
        hdr["A_0_2"] = -0.00015630563285242156
        hdr["A_0_3"] = -1.6225786855943367e-09
        hdr["A_1_1"] = -7.11405885212094e-06
        hdr["A_1_2"] = 6.641229215631077e-08
        hdr["A_2_0"] = -8.509167837901942e-05
        hdr["A_2_1"] = -2.858253312610882e-09
        hdr["A_3_0"] = 9.940844793780548e-08
        hdr["B_ORDER"] = 3
        hdr["B_0_2"] = 4.976793473695487e-06
        hdr["B_0_3"] = 8.709951903395686e-08
        hdr["B_1_1"] = 7.54764949893915e-05
        hdr["B_1_2"] = -1.0402281194607352e-09
        hdr["B_2_0"] = 7.503354619152504e-06
        hdr["B_2_1"] = 1.282581599768554e-07
        hdr["B_3_0"] = 2.44448533926602e-09
        hdr["AP_ORDER"] = 3
        hdr["BP_ORDER"] = 3

        return hdr
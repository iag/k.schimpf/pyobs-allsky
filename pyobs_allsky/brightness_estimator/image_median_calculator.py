import numpy as np


class ImageMedianCalculator:
    """Calculates the median brightness of an image inside a given mask"""
    def __init__(self, mask: np.ndarray, offset: float = 0.0, scaling_factor: float = 1.0):
        """
        Inits. the calculator.
        Args:
            mask: Mask defines the area, in which the average is calculated
            offset: Scaling factor for the brightness estimation (Brightness $b = \frac{\bar I}{\text{scaling}}$
            scaling_factor: Offset to the brightness estimation after scaling $b' = b - \text{offset}$
        """
        self._mask = mask
        self._offset = offset
        self._scaling_factor = scaling_factor

    def calc(self, image: np.ndarray):
        """
        Calculates the median image brightness
        Returns:
            Corrected median image brightness
        """
        return np.median(image[self._mask == 1])/self._scaling_factor-self._offset

import asyncio
import logging
from asyncio import Task
from typing import Any, Union, Optional, List

from pyobs.interfaces import IStartStop, ICamera
from pyobs.modules import Module

from pyobs_allsky.imaging_loop import ImagingLoop
from pyobs_allsky.imaging_loop.daytime_loop import DaytimeLoop
from pyobs_allsky.imaging_loop.daytime_waiter import DaytimeWaiter
from pyobs_allsky.imaging_loop.imaging_loop_factory import ImagingLoopFactory

log = logging.getLogger(__name__)


class AllSky(Module, IStartStop):
    """
    Controls a given camera as an all-sky camera
    """

    def __init__(self,
                 camera_name: str,
                 start_exposure_time: float,
                 min_exposure_time: float,
                 max_exposure_time: float,
                 start_gain: float,
                 min_gain: float,
                 max_gain: float,
                 mask_filepath: str,
                 brightness_offset: float,
                 brightness_scaling: int,
                 day: dict,
                 night: dict,
                 gain_converter_poly_params: Optional[List[float]] = None,
                 gain_inv_converter_poly_params: Optional[List[float]] = None,
                 **kwargs: Any):

        self._camera_name = camera_name

        self._imaging_loop_factory = ImagingLoopFactory(
                                                        start_exposure_time,
                                                        min_exposure_time,
                                                        max_exposure_time,
                                                        start_gain,
                                                        min_gain,
                                                        max_gain,
                                                        mask_filepath,
                                                        brightness_offset,
                                                        brightness_scaling,
                                                        gain_converter_poly_params,
                                                        gain_inv_converter_poly_params)

        self._day_config = day
        self._night_config = night

        self._day_loop: ImagingLoop = None
        self._night_loop: ImagingLoop = None
        self._imaging_task: Task = None

        super().__init__(**kwargs)

    async def open(self) -> None:
        """
        Opens the module and inits. the imaging loops.
        """
        await Module.open(self)

        camera = await self.proxy(self._camera_name, ICamera)

        self._imaging_loop_factory.load_mask()

        self._day_loop = self._create_image_loop_from_config(self._day_config, camera)
        self._night_loop = self._create_image_loop_from_config(self._night_config, camera)

        await self.start()

    def _create_image_loop_from_config(self, config, camera):
        self._imaging_loop_factory.set_image_broadcast(config["image_broadcast"])
        self._imaging_loop_factory.set_imaging_interval(config["imaging_interval"])
        self._imaging_loop_factory.set_image_saver(config["image_saver"])
        self._imaging_loop_factory.set_target_brightness_calculator(**config["brightness_target"])
        return self._imaging_loop_factory(camera, self.vfs)

    async def close(self) -> None:
        """
        Closes the module
        """
        await Module.close(self)

    async def start(self, **kwargs: Any) -> None:
        """Starts the imaging loop"""
        log.info("Started imaging loop")

        daytime_waiter = DaytimeWaiter(self.observer)
        loop = DaytimeLoop(self._day_loop, self._night_loop, daytime_waiter)
        self._imaging_task = asyncio.create_task(loop.start())

    async def stop(self, **kwargs: Any) -> None:
        """Stops the imaging loop"""
        log.info("Stopped imaging loop")
        if self._imaging_task is not None:
            self._imaging_task.cancel()
            self._imaging_task = None

    async def is_running(self, **kwargs: Any) -> bool:
        """Returns if the imaging loop is running"""
        return self._imaging_task is not None

import logging

from pyobs.images import Image

from pyobs_allsky.camera_settings.camera_setting import CameraSetting
from pyobs_allsky.camera_settings.calculator.secant_calculator import SecantCalculator
from pyobs_allsky.camera_settings.target_brightness_calculator import TargetBrightnessCalculator
from pyobs_allsky.imaging_iterator.camera_controller import CameraController
from pyobs_allsky.imaging_loop.catch_exception import catch_exception

log = logging.getLogger(__name__)


class ImagingIterator:
    """
    Wraps the CameraController, the TargetBrightnessCalculator, the exposure time/gain calculator (CameraSetting) and
    the TargetBrightnessCalculator together to yield images.
    """
    def __init__(self, camera_controller: CameraController,
                 exposure_time_setting: CameraSetting,
                 gain_setting: CameraSetting,
                 target_brightness_calculator: TargetBrightnessCalculator,
                 brightness_estimator,
                 ):
        self._camera: CameraController = camera_controller

        self._exposure_time_calculator = SecantCalculator(target_brightness_calculator, exposure_time_setting, gain_setting)

        self._brightness_estimator = brightness_estimator

        self._target_brightness_calculator = target_brightness_calculator

        self._current_brightness = 0.0
        self._image: Image = None

    def _get_camera_settings(self) -> (float, float):
        exposure_time = self._exposure_time_calculator.get_exposure_time()
        gain = self._exposure_time_calculator.get_gain()
        return exposure_time, gain

    async def _take_image(self, broadcast: bool):
        exposure_time, gain = self._get_camera_settings()
        log.info(f"Taking image with: Exposure Time: {exposure_time}s \t Gain: {gain}")
        await self._camera.apply_camera_settings(exposure_time, gain)
        self._image = await self._camera.take_image(broadcast)

    async def _run_correction(self, broadcast: bool):
        self._current_brightness = max(self._brightness_estimator.calc(self._image.data), 0)
        log.info(f"Mean brightness: {self._current_brightness}")
        self._exposure_time_calculator.calc(self._current_brightness)
        await self._take_image(broadcast)

    async def setup(self) -> None:
        """
        Takes images and corrects camera settings until the image brightness is acceptable.
        """
        await self._take_image(False)

        while not self._target_brightness_calculator.is_brightness_acceptable(self._current_brightness):
            if self._is_maxed_out():
                log.warning("Cannot reach target brightness since the camera setting boundary has been reached. "
                            "Continuing anyway...")
                break

            await self._run_correction(False)

    def _is_maxed_out(self):
        upper_end = (self._target_brightness_calculator.is_brightness_to_low(self._current_brightness) and
                     self._exposure_time_calculator.is_max_scaling_factor())
        lower_end = (self._target_brightness_calculator.is_brightness_to_high(self._current_brightness) and
                     self._exposure_time_calculator.is_min_scaling_factor())

        return upper_end or lower_end

    def __aiter__(self):
        return self

    @catch_exception(logger=logging.getLogger(__name__))
    async def __anext__(self) -> Image:
        await self._run_correction(True)
        return self._image

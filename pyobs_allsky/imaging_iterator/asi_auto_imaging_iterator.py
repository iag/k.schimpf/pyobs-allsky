import logging

from pyobs.images import Image

from pyobs_allsky.imaging_iterator.camera_controller import CameraController

log = logging.getLogger(__name__)


class AsiAutoImagingIterator:
    def __init__(self,  camera_controller: CameraController, start_exposure_time: float, start_gain: float):
        self._camera = camera_controller
        self._exposure_time = start_exposure_time
        self._gain = start_gain

    async def setup(self):
        log.info(f"Started image manager setup...")

        await self._camera.apply_camera_settings(self._exposure_time, self._gain)
        await self._camera.apply_camera_settings(-1, -1)

        log.info(f"Image manager setup complete...")

    async def next(self) -> Image:
        return await self._camera.take_image()

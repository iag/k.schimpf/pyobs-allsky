from typing import Callable

from pyobs.images import Image
from pyobs.interfaces import ICamera, IExposureTime, IGain, IImageFormat
from pyobs.utils.enums import ImageFormat
from pyobs.vfs import VirtualFileSystem


class CameraController:
    """
    Controls the settings and image taking of a pyobs camera
    """
    def __init__(self, camera: ICamera, vfs: VirtualFileSystem, broadcast: Callable):
        """
        Inits. the controller
        Args:
            camera: Camera
            vfs: Virtual file system
            broadcast: Function which returns true, if the image should be broadcast
        """
        self._camera = camera
        self._vfs = vfs
        self._broadcast = broadcast

    async def apply_camera_settings(self, exposure_time: float, gain: float) -> None:
        """
        Apply camera settings to camera.
        Notes:
            Fixed the image data type to uint16
        Args:
            exposure_time: Exposure time in seconds
            gain: Gain
        """
        if isinstance(self._camera, IExposureTime):
            await self._camera.set_exposure_time(exposure_time)

        if isinstance(self._camera, IGain):
            await self._camera.set_gain(gain)

        if isinstance(self._camera, IImageFormat):
            await self._camera.set_image_format(ImageFormat.INT16)

    async def take_image(self, broadcast: bool) -> Image:
        """
        Takes an image and broadcasts it according to the broadcast override and the broadcast object attribute
        Note:
            The broadcast override is necessary to prevent images taken while in the setup phase to be broadcast
        Args:
            broadcast: Broadcast override

        Returns:
            Taken image
        """
        image_name = await self._camera.grab_data(broadcast=self._broadcast() and broadcast)
        return await self._vfs.read_image(image_name)

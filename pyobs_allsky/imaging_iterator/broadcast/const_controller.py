
class ConstController:
    """
    Returns a constant value
    """
    def __init__(self, value: bool):
        self._value = value

    def __call__(self) -> bool:
        return self._value

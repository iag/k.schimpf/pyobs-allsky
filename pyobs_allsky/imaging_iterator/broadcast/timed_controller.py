import datetime


class TimedController:
    """
    Returns true, if the specified time delta passed after the last broadcast
    """
    def __init__(self, time_delta: int):
        self._time_delta: int = time_delta
        self._last_broad_casted: datetime.datetime = datetime.datetime.fromtimestamp(0)

    def __call__(self) -> bool:
        now = datetime.datetime.now()
        if (now - self._last_broad_casted).seconds >= self._time_delta:
            self._last_broad_casted = now
            return True

        return False

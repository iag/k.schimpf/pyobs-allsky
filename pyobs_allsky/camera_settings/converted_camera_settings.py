from typing import Optional, List

import numpy as np

from pyobs_allsky.camera_settings import CameraSetting


class ConvertedIntegerCameraSetting:
    """
    Camera setting wrapper which converts the integer setting value using a polynomial function
    """
    _IDENT_POLY = [0, 1]

    def __init__(self, unity_value: float, min_value: float, max_value: float,
                 converter_poly_params: Optional[List[float]] = None,
                 inv_converter_poly_params: Optional[List[float]] = None):
        self._camera_setting = CameraSetting(unity_value, min_value, max_value)

        if converter_poly_params is None or inv_converter_poly_params is None:
            converter_poly_params = self._IDENT_POLY
            inv_converter_poly_params = self._IDENT_POLY

        self._converter_poly = np.polynomial.Polynomial(converter_poly_params)
        self._inv_converter_poly = np.polynomial.Polynomial(inv_converter_poly_params)

    def set_converted(self, multiplier: float):
        """
        Set the converted value
        """
        target_value = self._converter_poly(multiplier)
        rounded_value = int(np.ceil(target_value))

        self._camera_setting.set_value(rounded_value)

    def set_value(self, value: float):
        self._camera_setting.set_value(value)

    def get_converted(self) -> float:
        """
        Get the converted value
        """
        value = self.get_value()
        multiplier = self._inv_converter_poly(value)
        return multiplier

    def get_value(self) -> float:
        """
        Get the original value
        """
        return self._camera_setting.get_value()

    def get_max_converted(self) -> float:
        """
        Get the maximum converted value
        """
        max_value = self._camera_setting.get_max_value()
        max_multiplier = self._inv_converter_poly(max_value)
        return max_multiplier

    def is_value_max(self):
        """
        Returns:
            If the value is at the upper bound
        """
        return self._camera_setting.is_value_max()

    def is_value_min(self):
        """
        Returns:
            If the value is at the lower bound
        """
        return self._camera_setting.is_value_min()

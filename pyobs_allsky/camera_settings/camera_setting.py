import numpy as np


class CameraSetting:
    """Representation of a camera setting with lower and upper bounds"""
    def __init__(self, value: float, min_value: float, max_value: float):
        self._value = value
        self._min_value = min_value
        self._max_value = max_value

        self._check_init_valid()

    def _check_init_valid(self):
        if not (self._min_value <= self._value <= self._max_value):
            raise ValueError("min_value <= value <= max_value!")

    def _apply_constraints(self):
        self._value = min(self._max_value, self._value)
        self._value = max(self._min_value, self._value)

    def set_value(self, new_value) -> None:
        """
        Set a new value under the constraint
        Args:
            new_value: New setting value
        """
        self._value = new_value
        self._apply_constraints()

    def get_value(self) -> float:
        """
        Gets the current setting value
        Returns:
            setting value
        """
        return self._value

    def calc_max_ratio(self) -> float:
        """
        Calculates the ratio between the upper bound and the current value
        Returns:
            ratio between the upper bound and the current value
        """
        return self._max_value/self._value

    def calc_min_ratio(self) -> float:
        """
        Calculates the ratio between the lower bound and the current value
        Returns:
            ratio between the lower bound and the current value
        """
        return self._min_value/self._value

    def is_value_max(self):
        """
        Returns:
            If the value is at the upper bound
        """
        return np.isclose(self._value, self._max_value)

    def is_value_min(self):
        """
        Returns:
            If the value is at the lower bound
        """
        return np.isclose(self._value, self._min_value)

    def get_max_value(self):
        """
        Gets the upper bound value
        Returns:
            upper bound value
        """
        return self._max_value

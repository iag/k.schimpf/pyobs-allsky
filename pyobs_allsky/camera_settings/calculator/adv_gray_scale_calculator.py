from pyobs_allsky.camera_settings.calculator.calculator import Calculator


class AdvGrayScaleCalculator(Calculator):
    """
    Advanced-Grayscale-Calculator
    """
    def _calc_multiplier(self, mean_brightness, target_brightness, aggressiveness_multiplier):
        return aggressiveness_multiplier * (1 - 1.5 * (mean_brightness - target_brightness)/mean_brightness)
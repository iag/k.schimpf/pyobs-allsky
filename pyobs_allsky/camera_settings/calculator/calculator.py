import logging
from abc import abstractmethod

from pyobs_allsky.camera_settings.camera_setting import CameraSetting
from pyobs_allsky.camera_settings.converted_camera_settings import ConvertedIntegerCameraSetting
from pyobs_allsky.camera_settings.target_brightness_calculator import TargetBrightnessCalculator

log = logging.getLogger(__name__)


class Calculator:
    """
    Calculator for gain and exposure time setting based on image brightness
    """
    def __init__(self,
                 target_brightness_calc: TargetBrightnessCalculator,
                 exposure_time_setting: CameraSetting,
                 gain_setting: ConvertedIntegerCameraSetting):

        self._target_brightness_calc = target_brightness_calc
        self._exposure_time_setting = exposure_time_setting
        self._gain_setting = gain_setting

    def calc(self, measured_brightness: float) -> None:
        """
        Calculate new camera settings based on the given brightness
        Args:
            measured_brightness: Measured brightness from the last image (mean/median)
        """
        if self._target_brightness_calc.is_brightness_acceptable(measured_brightness):
            return

        target_brightness, multiplier = self._target_brightness_calc.calc(measured_brightness)
        multiplier = self._calc_multiplier(measured_brightness, target_brightness, multiplier)

        new_scaling_factor = multiplier * self.get_scaling_factor()

        self._update_settings(new_scaling_factor)

    @abstractmethod
    def _calc_multiplier(self, mean_brightness, target_brightness, aggressiveness_multiplier):
        raise NotImplemented

    def _update_settings(self, scaling_factor: float):
        log.info(f"Updating scaling factor to {scaling_factor}")

        if scaling_factor > self._exposure_time_setting.get_max_value():
            target_gain = scaling_factor/self._exposure_time_setting.get_max_value()
            self._gain_setting.set_converted(target_gain)

            actual_gain = self._gain_setting.get_converted()
            exp_time = scaling_factor/actual_gain
            self._exposure_time_setting.set_value(exp_time)
        else:
            self._exposure_time_setting.set_value(scaling_factor)
            self._gain_setting.set_value(1.0)

    def get_scaling_factor(self) -> float:
        """
        Gets the scaling factor (exp_time * gain)
        """
        return self._exposure_time_setting.get_value() * self._gain_setting.get_converted()

    def get_max_scaling_factor(self) -> float:
        """
        Gets the maximum scaling factor
        """
        return self._exposure_time_setting.get_max_value() * self._gain_setting.get_max_converted()

    def is_min_scaling_factor(self):
        """
        If the scaling factor is minimal
        """
        return self._exposure_time_setting.is_value_min() and self._gain_setting.is_value_min()

    def is_max_scaling_factor(self):
        """
        If the scaling factor is maximal
        """
        return self._exposure_time_setting.is_value_max() and self._gain_setting.is_value_max()

    def get_exposure_time(self) -> float:
        """
        Gets the last calculated exposure time in seconds.
        Returns:
            exposure time
        """
        return self._exposure_time_setting.get_value()

    def get_gain(self) -> float:
        """
        Gets the last calculated gain.
        Returns:
            gain
        """
        return self._gain_setting.get_value()

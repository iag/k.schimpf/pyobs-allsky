from pyobs_allsky.camera_settings.calculator.calculator import Calculator


class FalsePositiveCalculator(Calculator):
    """
    False-Positive Calculator
    """
    def _calc_multiplier(self, mean_brightness, target_brightness, aggressiveness_multiplier):
        return aggressiveness_multiplier * target_brightness / mean_brightness

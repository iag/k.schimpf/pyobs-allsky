from pyobs_allsky.camera_settings.calculator.calculator import Calculator


class SecantCalculator(Calculator):
    """
    Secant Calculator (https://ieeexplore.ieee.org/document/7066300)
    """
    def _calc_multiplier(self, mean_brightness, target_brightness, aggressiveness_multiplier) -> float:
        max_brightness = 2**14
        diff_to_mean = target_brightness - mean_brightness
        diff_to_max = max_brightness - mean_brightness

        alpha = (max_brightness - target_brightness) * self.get_max_scaling_factor() * 1.1

        multiplier = diff_to_max/(diff_to_mean * self.get_scaling_factor() + alpha)

        return multiplier * self.get_max_scaling_factor() * 1.1

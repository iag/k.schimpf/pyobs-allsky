

class TargetBrightnessCalculator:
    """Calculates if a brightness is acceptable according to the config and the target brightness"""
    def __init__(self, acceptable_brightness, acceptable_deviation, aggressiveness):
        self._aggressiveness = aggressiveness
        self._set_acceptable_brightness(acceptable_brightness, acceptable_deviation)

    def _set_acceptable_brightness(self, acceptable_brightness, deviation):
        self._min_acceptable_brightness = max(0, acceptable_brightness - acceptable_brightness * deviation)
        self._max_acceptable_brightness = acceptable_brightness + acceptable_brightness * deviation

    def calc(self, measured_brightness) -> (float, float):
        """
        Calculates the target brightness and the aggressiveness multiplier based on the measured brightness.
        Args:
            measured_brightness: Measured brightness in the last image (mean/median)

        Returns:
            target brightness, aggressiveness multiplier
        """
        target_brightness: float
        aggressiveness_multiplier = self._aggressiveness

        if measured_brightness < self._min_acceptable_brightness:
            target_brightness = self._max_acceptable_brightness
        else:
            target_brightness = self._min_acceptable_brightness
            aggressiveness_multiplier = 1 / aggressiveness_multiplier

        return target_brightness, aggressiveness_multiplier

    def is_brightness_acceptable(self, brightness) -> bool:
        """
        Calculates if the given brightness inside the lower and upper bounds for the acceptable brightness
        Args:
            brightness: Last measured brightness

        Returns:
            If the brightness is acceptable
        """
        return self._min_acceptable_brightness < brightness < self._max_acceptable_brightness

    def is_brightness_to_low(self, brightness) -> bool:
        """
        Calculates if the given brightness is lower than the lower bound for the acceptable brightness
        Args:
            brightness: Last measured brightness

        Returns:
            If the brightness is lower than the lower bound for the acceptable brightness
        """
        return self._min_acceptable_brightness > brightness

    def is_brightness_to_high(self, brightness) -> bool:
        """
        Calculates if the given brightness is higher than the upper bound for the acceptable brightness
        Args:
            brightness: Last measured brightness

        Returns:
            If the brightness is higher than the upper bound for the acceptable brightness
        """
        return self._max_acceptable_brightness < brightness

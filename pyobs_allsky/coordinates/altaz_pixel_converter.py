from __future__ import annotations

import astropy.units as u
from astroplan import Observer
from astropy.coordinates import SkyCoord, Angle
from astropy.wcs import WCS
from pyobs.images import Image

from pyobs_allsky.coordinates.hourangle_converter import HourAngleConverter


class AltAzPixelConverter:
    """Converts between AltAz coordinates and pixel coordinates in an image"""
    def __init__(self, wcs: WCS, observer: Observer, hour_angle_converter: HourAngleConverter):
        self._wcs = wcs
        self._observer = observer
        self._hour_angle_converter = hour_angle_converter

    @staticmethod
    def from_image(image: Image) -> AltAzPixelConverter:
        """
        Creates a converter from an image header
        Args:
            image: Image to extract the header info from

        Returns:
            AltAzPixelConverter
        """
        wcs = WCS(image.header)

        observer = Observer(latitude=image.header["LATITUDE"] * u.deg, longitude=image.header["LONGITUD"] * u.deg,
                            elevation=image.header["HEIGHT"] * u.m)
        hour_angle_converter = HourAngleConverter(observer)

        return AltAzPixelConverter(wcs, observer, hour_angle_converter)

    def alt_az_to_pixel(self, coord: SkyCoord) -> tuple[float, float]:
        """
        Converts AltAz coordinates to pixel coordinates
        Notes:
            Observation time and location are overwritten with the objects attributes.
        Args:
            coord: AltAz coordinates

        Returns:
            pixel coordinates

        """
        coord = SkyCoord(alt=coord.alt, az=coord.az, frame="altaz",
                         obstime=coord.obstime,
                         location=self._observer.location)
        w = coord.transform_to("icrs")

        ha = self._hour_angle_converter(Angle(w.ra.deg * u.deg), w.obstime)
        w = SkyCoord(ra=ha, dec=w.dec)

        return self._wcs.world_to_pixel(w)

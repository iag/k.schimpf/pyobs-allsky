import datetime

from astroplan import Observer
from astropy.coordinates import Angle


class HourAngleConverter:
    """Converts between right ascension angle and hour angle"""
    def __init__(self, observer: Observer):
        self._observer = observer

    def __call__(self, ra: Angle, obstime: datetime.datetime) -> Angle:
        lst = self._observer.local_sidereal_time(obstime)
        lst = Angle(lst, unit='hourangle')

        return lst - ra

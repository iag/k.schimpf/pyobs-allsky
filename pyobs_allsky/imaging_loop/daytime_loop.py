import asyncio
import logging

from pyobs_allsky.imaging_loop import ImagingLoop
from pyobs_allsky.imaging_loop.catch_exception import catch_exception
from pyobs_allsky.imaging_loop.daytime_waiter import DaytimeWaiter

log = logging.getLogger(__name__)


class DaytimeLoop:
    """Loop which switches between the daytime and nighttime at sun-down/up"""
    _LOOP_NAMES = ["night", "day"]

    def __init__(self, daytime_loop: ImagingLoop, nighttime_loop: ImagingLoop, daytime_waiter: DaytimeWaiter):
        self._daytime_waiter = daytime_waiter
        self._loops = [nighttime_loop, daytime_loop]
        self._loop_index: int = int(self._daytime_waiter.get_sun_up())

    async def _task_canceller(self, loop_task: asyncio.Task):
        await self._daytime_waiter.wait()
        loop_task.cancel()

    async def _loop(self):
        log.info(f"Starting {self._LOOP_NAMES[self._loop_index]}-time loop...")
        loop_task = asyncio.create_task(self._loops[self._loop_index].start())

        await self._task_canceller(loop_task)
        log.info(f"Waiting {60}s for save cancellation...")
        await asyncio.sleep(60)

        self._loop_index = self._loop_index ^ 1

    @catch_exception(logger=logging.getLogger(__name__))
    async def start(self) -> None:
        """
        Starts the loop
        """
        while True:
            await self._loop()

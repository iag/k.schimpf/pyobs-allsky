import asyncio

from astroplan import Observer
from astropy.time import Time


class DaytimeWaiter:
    """Waits until the next switch between sun-up/down"""
    def __init__(self, observer: Observer):
        self._observer = observer
        self._sun_up = not observer.is_night(Time.now())

    def get_sun_up(self) -> bool:
        """
        Returns:
            If sun is up
        """
        return self._sun_up

    def _update_sun_up(self):
        self._sun_up = not self._sun_up

    def _get_next_time(self) -> Time:
        if self._sun_up:
            return self._observer.sun_set_time(Time.now())
        else:
            return self._observer.sun_rise_time(Time.now())

    def _get_sleep_time(self) -> int:
        return (self._get_next_time().to_datetime() - Time.now().to_datetime()).seconds

    async def wait(self) -> None:
        """
        Waits until the next sun-up/down
        """
        sleep_time = self._get_sleep_time()
        await asyncio.sleep(sleep_time)
        self._update_sun_up()


from typing import Union, Optional, List

import numpy as np
from pyobs.interfaces import ICamera
from pyobs.vfs import VirtualFileSystem

from pyobs_allsky.brightness_estimator.image_median_calculator import ImageMedianCalculator
from pyobs_allsky.camera_settings.camera_setting import CameraSetting
from pyobs_allsky.camera_settings.converted_camera_settings import ConvertedIntegerCameraSetting
from pyobs_allsky.camera_settings.target_brightness_calculator import TargetBrightnessCalculator
from pyobs_allsky.image.save.image_saver_factory import ImageSaverFactory
from pyobs_allsky.imaging_iterator.broadcast import TimedController, ConstController
from pyobs_allsky.imaging_iterator.camera_controller import CameraController
from pyobs_allsky.imaging_iterator.imaging_iterator import ImagingIterator
from pyobs_allsky.imaging_loop import ImagingLoop
from pyobs_allsky.imaging_loop.imaging_sleep_timer import ImagingSleepTimer


class ImagingLoopFactory:
    """Build imaging loops according to config"""
    def __init__(self,
                 start_exposure_time: float,
                 min_exposure_time: float,
                 max_exposure_time: float,
                 start_gain: float,
                 min_gain: float,
                 max_gain: float,
                 mask_filepath: str,
                 brightness_offset: float,
                 brightness_scaling: int,
                 gain_converter_poly_params: Optional[List[float]] = None,
                 gain_inv_converter_poly_params: Optional[List[float]] = None
                 ):

        self._image_broadcast = None
        self._imaging_sleep_timer = None

        self._exposure_time_setting = CameraSetting(start_exposure_time, min_exposure_time, max_exposure_time)
        self._gain_setting = ConvertedIntegerCameraSetting(
            start_gain, min_gain, max_gain, gain_converter_poly_params, gain_inv_converter_poly_params)

        self._target_brightness_calculator = None

        self._mask_filepath = mask_filepath
        self._brightness_offset = brightness_offset
        self._brightness_scaling = brightness_scaling
        self._mask: np.ndarray = None

        self._image_saver = []

    def set_image_broadcast(self, broadcast: Union[int, bool]) -> None:
        """
        Sets the image broadcast option. Use a timed controller, if int otherwise use a constant controller.
        Args:
            broadcast: broadcast option.
        """
        if isinstance(broadcast, bool):
            self._image_broadcast = ConstController(broadcast)
        elif isinstance(broadcast, int):
            self._image_broadcast = TimedController(broadcast)
        else:
            raise ValueError("Broadcast must be bool or int")

    def set_imaging_interval(self, imaging_interval: int) -> None:
        """
        Sets the imaging interval in seconds.
        Args:
            imaging_interval: imaging interval in seconds
        """
        self._imaging_sleep_timer = ImagingSleepTimer(imaging_interval)

    def set_image_saver(self, image_saver: list[dict]) -> None:
        """
        Constructs and sets the image saver given by the list of configs.
        Args:
            image_saver: List of image saver configs.
        """
        self._image_saver = []

        factory = ImageSaverFactory()
        for config in image_saver:
            factory.set_image_saver_config(config)
            factory.build()
            self._image_saver.append(factory.get_image_saver())

    def set_target_brightness_calculator(self, acceptable_brightness: float,
                                         acceptable_brightness_deviation: float, aggressiveness: float) -> None:
        """
        Sets the target brightness calculator option.
        Args:
            acceptable_brightness: Acceptable brightness value.
            acceptable_brightness_deviation: Acceptable brightness deviation fraction.
            aggressiveness: Aggressiveness multiplier
        """
        self._target_brightness_calculator = TargetBrightnessCalculator(acceptable_brightness,
                                                                        acceptable_brightness_deviation,
                                                                        aggressiveness)

    def load_mask(self) -> None:
        """
        Loads the mask from file.
        """
        self._mask = np.load(self._mask_filepath)

    def __call__(self, camera: ICamera, vfs: VirtualFileSystem):
        camera_controller = CameraController(camera, vfs, self._image_broadcast)
        brightness_estimator = ImageMedianCalculator(self._mask, self._brightness_offset, self._brightness_scaling)
        image_iterator = ImagingIterator(camera_controller,
                                         self._exposure_time_setting,
                                         self._gain_setting,
                                         self._target_brightness_calculator,
                                         brightness_estimator)

        return ImagingLoop(self._imaging_sleep_timer, image_iterator, self._image_saver)

import logging


def catch_exception(exception=Exception, logger=logging.getLogger(__name__)):
    def decorator(func):
        async def wrapper(*args, **kwargs):
            try:
                result = await func(*args, **kwargs)
            except exception as e:
                logger.exception(e)
            else:
                return result
        return wrapper
    return decorator

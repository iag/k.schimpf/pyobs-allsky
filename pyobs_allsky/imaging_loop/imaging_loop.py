import logging

from pyobs.images import Image

from pyobs_allsky.image.save import ImageSaver
from pyobs_allsky.imaging_loop.catch_exception import catch_exception
from pyobs_allsky.imaging_loop.imaging_sleep_timer import ImagingSleepTimer

logger = logging.getLogger(__name__)


class ImagingLoop:
    """Wraps ImageIterator and takes images according to ImagingSleepTimer and saves them using ImageSaver"""
    def __init__(self, imaging_sleep_timer: ImagingSleepTimer, image_iterator, image_saver: list[ImageSaver]):
        self._imaging_sleep_timer = imaging_sleep_timer
        self._image_iterator = image_iterator
        self._image_saver = image_saver

    def _save(self, image: Image):
        for s in self._image_saver:
            s.save(image)

    async def _loop(self):
        try:
            image = await self._image_iterator.__anext__()
            self._save(image)
            await self._imaging_sleep_timer(image)
        except Exception as e:
            logger.exception(e)

    @catch_exception(logger=logging.getLogger(__name__))
    async def start(self) -> None:
        """
        Start the imaging loop

        Args:
            setup: Run the setup loop before starting
        """

        await self._image_iterator.setup()

        while True:
            await self._loop()

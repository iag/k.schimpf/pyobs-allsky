import asyncio
import logging

from pyobs.images import Image


log = logging.getLogger()


class ImagingSleepTimer:
    """
    Sleeps according to the imaging interval and the last exposure time.
    Total sleep time is (imaging interval - exp. time).
    """
    def __init__(self, imaging_interval: float):
        """
        Inits. sleep timer
        Args:
            imaging_interval: imaging interval in seconds.
        """
        self._imaging_interval = imaging_interval

    def _calc_sleep_time(self, image: Image) -> float:
        exp_time = float(image.header["EXPTIME"])
        return max(0.0, self._imaging_interval - exp_time)

    async def __call__(self, image: Image):
        time = self._calc_sleep_time(image)
        log.debug(f"Sleeping for {time}s")

        await asyncio.sleep(time)

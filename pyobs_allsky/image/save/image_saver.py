import os
from typing import Callable

from pyobs.images import Image

from pyobs_allsky.image.file_path_creator import FilePathCreator
from pyobs_allsky.image.save.pipeline.pipeline_runner import PipelineRunner
from pyobs_allsky.image.save.save_strategy import SaveStrategy


class ImageSaver:
    """
    Wrapper class, executes the given strategies to save a file
    """
    def __init__(self,
                 file_path_creator: FilePathCreator,
                 saving_strategy: SaveStrategy,
                 pipeline: PipelineRunner,
                 timer: Callable):

        self._file_path_creator = file_path_creator
        self._saving_strategy = saving_strategy
        self._pipeline = pipeline
        self._timer: Callable = timer

    def _generate_file_path(self, image: Image) -> os.path:
        self._file_path_creator.update(image)
        self._file_path_creator.create()
        return self._file_path_creator.get_path()

    def save(self, image: Image) -> None:
        """
        Processes an image and saves the result using the saving strategy.
        Args:
            image: Image to save
        """
        if self._timer():
            processed_image = self._pipeline.run(image)
            file_path = self._generate_file_path(processed_image)
            self._saving_strategy.save(processed_image, file_path)

import exif
from pyobs.images import Image

from pyobs_allsky.image.save.jpg.jpg_saver import JPGSaver
from pyobs_allsky.image.save.jpg.encoder import Encoder
from pyobs_allsky.image.save.save_strategy import SaveStrategy


class JPGStrategy(SaveStrategy):
    """Saves an image as a jpg file"""
    def __init__(self):
        """
        Inits. the JPG strategy.
        """
        self._encoder = Encoder()
        self._saver = JPGSaver()

    def _encode(self, image: Image) -> exif.Image:
        self._encoder.set_image(image)
        self._encoder.encode()
        return self._encoder.get_image()

    def _save(self, image: exif.Image, path: str):
        self._saver.set_image(image)
        self._saver.save(path)

    def save(self, image: Image, path: str) -> None:
        """
        Saves the given image to the specified path
        Args:
            image: Image to save
            path: Path to save the image to
        """
        compressed_image = self._encode(image)
        self._save(compressed_image, path)


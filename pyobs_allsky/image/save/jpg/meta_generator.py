import datetime

from pyobs.images import Image


class MetaGenerator:
    """Generators JPG Meta info from fits image."""
    def __init__(self, image: Image):
        """
        Inits. the generator with the image
        Args:
            image: Image to extract the header info from
        """
        self._header = image.header

    def _get_datetime_str(self) -> str:
        datetime_obs = datetime.datetime.strptime(self._header["DATE-OBS"], "%Y-%m-%dT%H:%M:%S.%f")
        return datetime_obs.strftime("%Y:%m:%d %H:%M:%S")

    def __call__(self) -> dict:
        meta = {
            "artist": "IAG",
            "exposure_time": self._header["EXPTIME"],
            "datetime": self._get_datetime_str()
        }

        return meta

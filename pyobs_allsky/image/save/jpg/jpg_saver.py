import exif


class JPGSaver:
    """
    Saves a jpg encoded image.
    """
    def __init__(self):
        """
        Inits. the saver.
        """
        self._image: exif.Image = None

    def set_image(self, image: exif.Image) -> None:
        """
        Sets the image to save
        Args:
            image: Image to save
        """
        self._image = image

    def save(self, path: str) -> None:
        """
        Saves the current image to the specified path
        Args:
            path: Path to save the image to
        """
        with open(path, "wb") as new_image_file:
            new_image_file.write(self._image.get_file())
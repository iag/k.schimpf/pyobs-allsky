import cv2
import exif
from pyobs.images import Image
from pyobs_allsky.image.save.jpg.meta_generator import MetaGenerator


class Encoder:
    """
    Encodes an image and header data into jpg
    """
    def __init__(self):
        """
        Inits the encoder.
        """
        self._image = None
        self._meta_generator: MetaGenerator = None

    def _normalize(self) -> None:
        image = self._image.data.copy()
        normalised_image = cv2.normalize(image, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
        self._image.data = normalised_image

    def _convert_to_exif(self) -> None:
        _, encoded_image = cv2.imencode('.jpg', self._image.data)
        byte_image = encoded_image.tobytes()
        self._image = exif.Image(byte_image)

    def _write_meta_to_image(self, meta) -> None:
        for key, value in meta.items():
            self._image[key] = value

    def set_image(self, image: Image) -> None:
        """
        Sets the image to encode
        Args:
            image: Input image
        """
        self._image = image
        self._meta_generator = MetaGenerator(image)

    def encode(self) -> None:
        """
        Encodes the current image
        """
        self._normalize()
        self._convert_to_exif()
        meta = self._meta_generator()
        self._write_meta_to_image(meta)

    def get_image(self) -> exif.Image:
        """
        Gets the encoded image. Resets the state of the encoder.
        Returns:
            Encoded image
        """
        image = self._image
        self._image = None
        self._meta_generator = None
        return image

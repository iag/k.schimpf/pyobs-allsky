from pyobs_allsky.image.save.image_saver import ImageSaver

from pyobs_allsky.image.save.jpg.jpg_strategy import JPGStrategy
from pyobs_allsky.image.save.fits_strategy import FitsStrategy

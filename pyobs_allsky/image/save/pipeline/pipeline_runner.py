from pyobs.images import Image

from .processor.processor import Processor


class PipelineRunner:
    """
    Runs a pipeline (list) of image processors
    """
    def __init__(self, pipeline: list[Processor]):
        """
        Inits pipline with list of processors
        Args:
            pipeline: List of (image) processors
        """
        self._pipeline = pipeline

    def run(self, image: Image) -> Image:
        """
        Runs the image processors in seq. order on the given image.
        Args:
            image: Image to process.

        Returns:
            Processed image
        """
        working_image = image.copy()
        for processor in self._pipeline:
            working_image = processor.process(working_image)

        return working_image

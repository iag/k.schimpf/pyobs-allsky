from abc import ABC, abstractmethod

from pyobs.images import Image


class Processor(ABC):
    """
    Processes an image. Used as a part of a pipeline.
    """
    @abstractmethod
    def process(self, image: Image) -> Image:
        """
        Processes an image
        Args:
            image: Image to process

        Returns:
            Processed image
        """
        raise NotImplemented

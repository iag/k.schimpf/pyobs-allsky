import cv2
import numpy as np
from pyobs.images import Image

from pyobs_allsky.image.save.pipeline.processor import Processor


class DeBayerer(Processor):
    """Debayers an image"""
    @staticmethod
    def _de_bayer(image: Image) -> Image:
        raw_image = image.data.astype(np.uint16)
        de_bayered_image = cv2.cvtColor(raw_image, cv2.COLOR_BAYER_BG2BGR)
        image.data = de_bayered_image
        return image

    @staticmethod
    def _normalize(image: Image) -> Image:
        normalised_image = cv2.normalize(image.data, None, alpha=0, beta=2**16-1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_16U)
        image.data = normalised_image
        return image

    def process(self, image: Image) -> Image:
        """
        Debayers an image
        Args:
            image: Image to debayer

        Returns:
            Debayered (RGB) image

        """
        raw_image = image.copy()
        de_bayered_image = self._de_bayer(raw_image)
        normalized_image = self._normalize(de_bayered_image)
        return normalized_image

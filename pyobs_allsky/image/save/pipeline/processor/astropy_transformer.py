from typing import Union

from pyobs.images import Image
from pyobs.object import get_object

from astropy.visualization import BaseStretch, BaseInterval

from pyobs_allsky.image.save.pipeline.processor import Processor


class AstropyTransformer(Processor):
    def __init__(self, config: dict):
        self._transformer: Union[BaseStretch, BaseInterval] = get_object(config, Union[BaseStretch, BaseInterval])

    def process(self, image: Image) -> Image:
        raw_image = image.copy()
        raw_image.data = self._transformer(raw_image.data)

        return raw_image

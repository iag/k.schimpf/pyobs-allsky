import datetime

import cv2
import numpy as np
from pyobs.images import Image

from pyobs_allsky.image.save.pipeline.processor import Processor


class HeaderWriter(Processor):
    """
    Writes header key-value pairs onto the image
    """

    def __init__(self, header_keys: list[str]):
        """
        Inits. the writer with the header keys.
        Args:
            header_keys: Header keys to write onto an image
        """
        self._header_keys = header_keys

    @staticmethod
    def _gen_date(image: Image) -> str:
        datetime_obs = datetime.datetime.strptime(image.header["DATE-OBS"], "%Y-%m-%dT%H:%M:%S.%f")
        return datetime_obs.strftime("%d.%m.%Y %H:%M:%S")

    def _format_header_value(self, image: Image, key: str):
        if key == "DATE-OBS":
            return self._gen_date(image)

        if isinstance(image.header[key], float):
            value = np.format_float_positional(image.header[key], precision=2, unique=False, fractional=False, trim='k')
        else:
            value = image.header[key]

        return f"{key}: {value}"

    def _gen_header_text(self, image: Image) -> list[str]:
        return [self._format_header_value(image, key) for key in self._header_keys]

    @staticmethod
    def _write_header_on_image(header_text: list[str], image: Image) -> Image:
        rgb_color = (2 ** 16 - 1, 2 ** 16 - 1, 2 ** 16 - 1)
        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 1.5
        thickness = 2
        org = (50, 75)

        for n, text in enumerate(header_text):
            image.data = cv2.putText(image.data, text, (org[0], org[1] + n * 50), font, font_scale, rgb_color,
                                     thickness, cv2.LINE_AA)

        return image

    def process(self, image: Image) -> Image:
        """
        Writes header key-value pairs onto the image
        Args:
            image: Image to write to

        Returns:
            Image with header key-value pairs onto the image

        """
        raw_image = image.copy()
        header_text = self._gen_header_text(raw_image)
        annotated_image = self._write_header_on_image(header_text, raw_image)

        return annotated_image

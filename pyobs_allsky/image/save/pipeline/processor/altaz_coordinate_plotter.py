import datetime
from copy import copy

import astropy.units as u
import cv2
import numpy as np
from astropy.coordinates import SkyCoord
from pyobs.images import Image

from pyobs_allsky.coordinates import AltAzPixelConverter
from pyobs_allsky.image.save.pipeline.processor import Processor


class AltAzCoordinatePlotter(Processor):
    """
    Plots an alt az coordinate grid onto an image
    """
    _ALT_TICKS = np.linspace(10, 70, 3)
    _AZ_TICKS = np.arange(0, 180, 45)
    _Y_PAD = 100
    _COLOR = (2**16 - 1, 2**16 - 1, 2**16 - 1)

    def __init__(self, lod: float = 1.0):
        """
        Inits. the plotter with a level of detail.
        Args:
            lod: Level of detail of the coordinate grid
        """
        self._datetime = datetime.datetime.now()
        self._converter: AltAzPixelConverter = None
        self._image = None

        self._lod = lod

        self._canvas = None

    def _az_lines(self):
        alt = np.linspace(10, 90, int(90 * self._lod))

        for az in self._AZ_TICKS:
            world_coords = [SkyCoord(az=az * u.deg, alt=a * u.deg, frame="altaz", obstime=self._datetime) for a in alt]
            world_coords += [SkyCoord(az=(az + 180) * u.deg, alt=a * u.deg, frame="altaz", obstime=self._datetime) for a
                             in alt]

            pixel_coords = [self._converter.alt_az_to_pixel(w) for w in world_coords]
            filtered_coords = self._filter_pixel_coordinates(pixel_coords)

            for i in range(len(filtered_coords) // 2):
                self._canvas = cv2.line(self._canvas, filtered_coords[2 * i], filtered_coords[2 * i + 1],
                                        lineType=cv2.LINE_AA, thickness=2,
                                        color=self._COLOR)

    def _alt_lines(self):
        az = np.linspace(0, 360, int(180 * self._lod))
        for alt in self._ALT_TICKS:
            world_coords = [SkyCoord(alt=alt * u.deg, az=a * u.deg, frame="altaz", obstime=self._datetime) for a in az]
            pixel_coords = [self._converter.alt_az_to_pixel(w) for w in world_coords]
            filtered_coords = np.array(self._filter_pixel_coordinates(pixel_coords))
            points = filtered_coords.reshape((-1, 1, 2))

            self._canvas = cv2.polylines(self._canvas, [points], isClosed=True, lineType=cv2.LINE_AA, thickness=3,
                                         color=self._COLOR)

    def _alt_labels(self, az=180):
        world_coords = [SkyCoord(az=az * u.deg, alt=a * u.deg, frame="altaz", obstime=self._datetime) for a in
                        self._ALT_TICKS]

        pixel_coords = [self._converter.alt_az_to_pixel(w) for w in world_coords]
        px, py = tuple(map(list, zip(*pixel_coords)))

        for x, y, d in zip(px, py, self._ALT_TICKS):
            self._canvas = cv2.putText(self._canvas, f"{int(d)}", [int(x) + 10, int(y) + 50 + self._Y_PAD], color=self._COLOR,
                                       fontFace=cv2.FONT_HERSHEY_DUPLEX, fontScale=1.5, thickness=2)
            self._canvas = cv2.circle(self._canvas, [int(x) + 80, int(y) + 20 + self._Y_PAD], 5, color=self._COLOR, thickness=2)

    def _az_labels(self):
        w = SkyCoord(az=0.0 * u.deg, alt=9 * u.deg, frame="altaz", obstime=self._datetime)
        px, py = self._converter.alt_az_to_pixel(w)
        cv2.putText(self._canvas, "N", [int(px), int(py) + 30 + self._Y_PAD], color=self._COLOR,
                    fontFace=cv2.FONT_HERSHEY_DUPLEX, fontScale=3, thickness=3)

        w = SkyCoord(az=180.0 * u.deg, alt=5 * u.deg, frame="altaz", obstime=self._datetime)
        px, py = self._converter.alt_az_to_pixel(w)
        cv2.putText(self._canvas, "S", [int(px) - 10, int(py) + 30 + self._Y_PAD], color=self._COLOR,
                    fontFace=cv2.FONT_HERSHEY_DUPLEX, fontScale=3, thickness=3)

        w = SkyCoord(az=90.0 * u.deg, alt=25 * u.deg, frame="altaz", obstime=self._datetime)
        px, py = self._converter.alt_az_to_pixel(w)
        cv2.putText(self._canvas, "E", [int(px) - 20, int(py) + self._Y_PAD], color=self._COLOR,
                    fontFace=cv2.FONT_HERSHEY_DUPLEX, fontScale=3, thickness=3)

        w = SkyCoord(az=270.0 * u.deg, alt=32 * u.deg, frame="altaz", obstime=self._datetime)
        px, py = self._converter.alt_az_to_pixel(w)
        cv2.putText(self._canvas, "W", [int(px) - 40, int(py) + self._Y_PAD], color=self._COLOR,
                    fontFace=cv2.FONT_HERSHEY_DUPLEX, fontScale=3, thickness=3)

    def _pixel_coordinate_in_image(self, px, py):
        return (0 <= px < np.shape(self._image.data)[1]) and (0 <= py < np.shape(self._image.data)[0])

    def _filter_pixel_coordinates(self, pixel_coords: list[tuple[float, float]]):
        return [[int(c[0]), int(c[1])+self._Y_PAD] for c in pixel_coords if self._pixel_coordinate_in_image(*c)]

    def _plot_coordinate_system(self):
        self._alt_lines()
        self._alt_labels()

        self._az_lines()
        self._az_labels()

    def _init_canvas(self):
        shape = np.shape(self._image.data)
        self._canvas = np.zeros((shape[0] + 2*self._Y_PAD, shape[1], shape[2]))

    def _apply_canvas(self):
        self._image.data = np.clip(self._image.data + self._canvas, 0, 2**16 - 1)

    def _flip_image(self):
        self._image.data = np.flip(self._image.data, axis=0)

    def _pad_image(self):
        self._image.data = np.pad(self._image.data, [(self._Y_PAD, self._Y_PAD), (0, 0), (0, 0)], mode='constant', constant_values=0)

    def _norm_image(self):
        self._image.data = cv2.normalize(self._image.data, None, alpha=0, beta=2**16 - 1, norm_type=cv2.NORM_MINMAX,
                                         dtype=cv2.CV_16U)

    def process(self, image: Image) -> Image:
        """
        Plots an alt az coordinate grid onto the image.
        Notes:
              Coordinate grid is cached on the first call and then reapplied onto
              every other image.
        Args:
            image: Image on which the coordinate grid should be plotted

        Returns:
            Image with coordinate grid
        """
        self._image = copy(image)

        if self._canvas is None:
            self._init_canvas()
            self._converter = AltAzPixelConverter.from_image(self._image)
            self._plot_coordinate_system()

        self._flip_image()
        self._pad_image()
        self._norm_image()

        self._apply_canvas()

        return self._image

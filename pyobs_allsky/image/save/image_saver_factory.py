from pyobs.object import get_object

from pyobs_allsky.image.file_path_creator import FilePathCreator
from pyobs_allsky.image.save import ImageSaver
from pyobs_allsky.image.save.save_strategy import SaveStrategy
from pyobs_allsky.image.save.pipeline.processor import Processor
from pyobs_allsky.image.save.pipeline import PipelineRunner
from pyobs_allsky.imaging_iterator.broadcast import TimedController, ConstController


class ImageSaverFactory:
    """
    Builds an ImageSaver based on a config
    """
    def __init__(self):
        self._image_saver_config = None
        self._image_saver: ImageSaver = None

    def set_image_saver_config(self, config) -> None:
        """
        Set an image saver config.
        Args:
            config: Image saver config
        """
        self._image_saver_config = config

    def build(self) -> None:
        """
        Build an image saver according to the config
        """
        directory = FilePathCreator(self._image_saver_config["file_path"])
        saving_strategy = get_object(self._image_saver_config["saving_strategy"], SaveStrategy)

        pipeline = [get_object(process_config, Processor) for process_config in self._image_saver_config["pipeline"]]
        pipeline_runner = PipelineRunner(pipeline)

        if "time_btw_saves" in self._image_saver_config:
            timer = TimedController(self._image_saver_config["time_btw_saves"])
        else:
            timer = ConstController(True)

        self._image_saver = ImageSaver(directory, saving_strategy, pipeline_runner, timer)

    def get_image_saver(self) -> ImageSaver:
        """
        Get build image saver
        Returns:
            Image saver
        """
        return self._image_saver

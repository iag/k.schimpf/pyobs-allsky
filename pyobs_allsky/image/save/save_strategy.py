from abc import abstractmethod, ABC

from pyobs.images import Image


class SaveStrategy(ABC):
    """
    Strategy how to save an image
    """

    @abstractmethod
    def save(self, image: Image, path: str) -> None:
        """
        Saves an image.
        Args:
            image: Image to save.
            path: Path to save the image to
        """
        raise NotImplemented

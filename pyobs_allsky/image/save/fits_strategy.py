import logging

from pyobs.images import Image

from pyobs_allsky.image.save.save_strategy import SaveStrategy

log = logging.getLogger(__name__)


class FitsStrategy(SaveStrategy):
    """
    Saves a fits image to file
    """
    def __init__(self, overwrite=False):
        """
        Inits strategy with setting the overwrite config value
        Args:
            overwrite: If a file is allowed to be overwritten
        """
        self._overwrite = overwrite

    def save(self, image: Image, path: str) -> None:
        """
        Saves a fits image to file
        Args:
            image: Image to save
            path: Path to save the image to
        """
        image.writeto(path, overwrite=self._overwrite)

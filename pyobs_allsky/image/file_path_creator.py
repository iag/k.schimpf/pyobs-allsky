import datetime
import os

from pyobs.images import Image


class FilePathCreator:
    """Creates a directory path string based on a given format"""
    _DATE_OBS_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"

    def __init__(self, path_format: str):
        """
        Inits DirectoryPathCreator with a directory path format.
        Args:
            path_format: Format of the path. Datetime directives are allowed.
        """
        self._path_format = path_format
        self._path = None

    def _extract_date(self, image: Image) -> datetime.datetime:
        return datetime.datetime.strptime(image.header["DATE-OBS"], self._DATE_OBS_FORMAT)

    def _format_path(self, date_obs: datetime.datetime):
        self._path = date_obs.strftime(self._path_format)

    def get_path(self) -> str:
        """
        Gets the current path
        Returns:
            Current path
        """
        return self._path

    def update(self, image: Image):
        """
        Updates the path according to the given image
        Args:
            image: Image
        """
        date = self._extract_date(image)
        self._format_path(date)

    def create(self):
        """
        Creates the directory corresponding to the current path
        """
        os.makedirs(os.path.dirname(self._path), exist_ok=True)



Pyobs AllSky
==============
[![pipeline status](https://gitlab.gwdg.de/iag/k.schimpf/pyobs-allsky/badges/main/pipeline.svg)](https://gitlab.gwdg.de/iag/k.schimpf/pyobs-allsky/-/commits/main)
[![coverage report](https://gitlab.gwdg.de/iag/k.schimpf/pyobs-allsky/badges/main/coverage.svg)](https://gitlab.gwdg.de/iag/k.schimpf/pyobs-allsky/-/commits/main)
[![docstr coverage](https://gitlab.gwdg.de/api/v4/projects/30396/jobs/artifacts/main/raw/docstr-badge.svg?job=docstr-cov)](https://gitlab.gwdg.de/iag/k.schimpf/pyobs-allsky/-/commits/main)
[![Latest Release](https://gitlab.gwdg.de/iag/k.schimpf/pyobs-allsky/-/badges/release.svg)](https://gitlab.gwdg.de/iag/k.schimpf/pyobs-allsky/-/releases) 

This module is design to autonomously manage an Allsky camera.

## Features
- Image brightness control through exposure time and gain setting management
- Imaging cadence control
- Day and night image pipeline

## Config values
### General
| Name                | Description                                                                                 | Type               |
|---------------------|---------------------------------------------------------------------------------------------|--------------------|
| camera_name         | pyobs user name of the camera                                                               | *String*           |

### Camera Settings
| Name                | Description                                                                                 | Type               |
|---------------------|---------------------------------------------------------------------------------------------|--------------------|
| start_exposure_time | Exposure time (in sec.) to start the auto exposure time algorithm with                      | *Float*            |
| min_exposure_time   | Minimum allowed exposure time (in sec.)                                                     | *Float*            |
| max_exposure_time   | Maximum allowed exposure time (in sec.)                                                     | *Float*            |
| start_gain          | Gain setting to start the auto exposure time algorithm with                                 | *Float*            |
| min_gain            | Minimum allowed gain setting                                                                | *Float*            |
| max_gain            | Maximum allowed gain setting                                                                | *Float*            |

### Brightness
| Name                            | Description                                                                                  | Type      |
|---------------------------------|----------------------------------------------------------------------------------------------|-----------|
| mask_filepath                   | Filepath to the image mask, defining the area, from which the image brightness is sampled    | *String*  |
| brightness_scaling              | Scaling factor for the brightness estimation (Brightness $b = \frac{\bar I}{\text{scaling}}$ | *Integer* |
| brightness_offset               | Offset to the brightness estimation after scaling $b' = b - \text{offset}$                   | *Integer* |

### Day/Night Config
#### General
| Name                | Description                                                                                 | Type               |
|---------------------|---------------------------------------------------------------------------------------------|--------------------|
| image_broadcast     | Time (in sec.) between broadcasts if integer, enabled/disabled if bool                      | *Integer* / *Bool* |
| imaging_interval    | Period (in sec.) of imaging frequency                                                       | *Integer*          |

#### Brightness Target

| Name                            | Description                                                      | Type      |
|---------------------------------|------------------------------------------------------------------|-----------|
| acceptable_brightness           | Target brightness for the auto exposure time algorithm           | *Integer* |
| acceptable_brightness_deviation | Acceptable deviation from the target brightness (deadzone)       | *Integer* |
| aggressiveness                  | Scaling factor for the resulting exposure time correction factor | *Float*   |

#### Image Saver
class: `pyobs_allsky.image.save.ImageSaver`

| Name            | Description                                                                                                                                                           | Type     |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| file_path       | Filepath, where the images should be saved to. Allows for [`strftime` directives](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes) | *String* |
| saving_strategy | How to save the image. (`pyobs_allsky.image.save.JPGStrategy` or `pyobs_allsky.image.save.FitsStrategy`)                                                              | *Class*  |
| time_btw_saves  | Time in seconds between saved images (optional)                                                                                                                       |          |

##### Pipeline
Option: `pipline` <br>
Description: List of image processors to apply to the image before saving 

###### De-Bayerer
class: `pyobs_allsky.image.save.pipeline.DeBayerer` <br>
Description: If the camera uses a bayer filter, this processor produces a rgb image from the b/w image.

###### Header Writer
class: `pyobs_allsky.image.save.pipeline.HeaderWriter` <br>
Description: Writes header keys onto the image

| Name        | Description                         | Type           |
|-------------|-------------------------------------|----------------|
| header_keys | Header keys to write onto the image | *list[String]* |

###### AltAz Coordinate Plotter
class: `pyobs_allsky.image.save.pipeline.AstropyTransformer` <br>
Description: Applies an astropy transformer to the image

| Name   | Description                                           | Type     |
|--------|-------------------------------------------------------|----------|
| config | Config which describes the astropy transformer object | *dict*   |

###### AltAz Coordinate Plotter
class: `pyobs_allsky.image.save.pipeline.AltAzCoordinatePlotter` <br>
Description: Plots a AltAz Coordinate System onto the image
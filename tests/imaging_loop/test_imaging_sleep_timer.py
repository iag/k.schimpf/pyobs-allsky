import asyncio

import pytest

from pyobs_allsky.imaging_loop.imaging_sleep_timer import ImagingSleepTimer


def test_init():
    timer = ImagingSleepTimer(10.0)
    assert timer._imaging_interval == 10.0


def test_calc_sleep_time(mock_image_header):
    timer = ImagingSleepTimer(10.0)

    mock_image_header.header["EXPTIME"] = 1.0
    assert timer._calc_sleep_time(mock_image_header) == 9.0

    mock_image_header.header["EXPTIME"] = 11.0
    assert timer._calc_sleep_time(mock_image_header) == 0.0


@pytest.mark.asyncio
async def test_call(mocker, mock_image_header):
    timer = ImagingSleepTimer(10.0)

    mocker.patch.object(timer, "_calc_sleep_time", return_value=1.0)
    mocker.patch("asyncio.sleep")

    await timer(mock_image_header)

    asyncio.sleep.assert_called_once_with(1.0)

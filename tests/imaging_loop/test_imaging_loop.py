import pytest

from pyobs_allsky.imaging_loop import ImagingLoop
from pyobs_allsky.imaging_loop.imaging_sleep_timer import ImagingSleepTimer


@pytest.fixture()
def imaging_sleep_timer():
    return ImagingSleepTimer(30.0)


def test_init(imaging_sleep_timer, image_iterator, fits_saver, compressed_saver):
    image_saver = [fits_saver, compressed_saver]
    loop = ImagingLoop(imaging_sleep_timer, image_iterator, image_saver)

    assert loop._imaging_sleep_timer == imaging_sleep_timer
    assert loop._image_iterator == image_iterator
    assert loop._image_saver == image_saver


def test_save(mocker, imaging_sleep_timer, image_iterator, fits_saver, compressed_saver, mock_image_header):
    mocker.patch.object(fits_saver, "save")
    mocker.patch.object(compressed_saver, "save")

    loop = ImagingLoop(imaging_sleep_timer, image_iterator, [fits_saver, compressed_saver])

    loop._save(mock_image_header)

    fits_saver.save.assert_called_once_with(mock_image_header)
    compressed_saver.save.assert_called_once_with(mock_image_header)


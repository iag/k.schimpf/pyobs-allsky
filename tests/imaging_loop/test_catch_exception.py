import logging

import pytest

from pyobs_allsky.imaging_loop.catch_exception import catch_exception


@catch_exception()
async def mock_working_function():
    return 0


@catch_exception()
async def mock_exception_function():
    raise ValueError


@pytest.mark.asyncio()
async def test_working_function():
    assert await mock_working_function() == 0


@pytest.mark.asyncio()
async def test_exception_function(mocker):
    mocker.patch("logging.Logger.exception")
    await mock_exception_function()
    logging.Logger.exception.assert_called_once()
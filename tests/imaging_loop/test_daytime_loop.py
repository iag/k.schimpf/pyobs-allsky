import pytest

from pyobs_allsky.imaging_loop.daytime_loop import DaytimeLoop
from pyobs_allsky.imaging_loop.daytime_waiter import DaytimeWaiter


class MockImagingLoop:
    async def start(self, *args, **kwargs):
        ...


@pytest.fixture()
def mock_daytime_waiter(mocker, observer):
    waiter = DaytimeWaiter(observer)
    mocker.patch.object(waiter, "wait")
    mocker.patch.object(waiter, "get_sun_up", return_value=True)

    return waiter


def test_init(mock_daytime_waiter):
    daytime_loop = MockImagingLoop()
    nighttime_loop = MockImagingLoop()
    loop = DaytimeLoop(daytime_loop, nighttime_loop, mock_daytime_waiter)

    assert loop._daytime_waiter == mock_daytime_waiter
    assert loop._loops[0] == nighttime_loop
    assert loop._loops[1] == daytime_loop
    assert loop._loop_index == 1


@pytest.mark.asyncio()
async def test_loop(mocker, mock_daytime_waiter):
    mocker.patch("asyncio.sleep")
    daytime_loop = MockImagingLoop()
    nighttime_loop = MockImagingLoop()
    loop = DaytimeLoop(daytime_loop, nighttime_loop, mock_daytime_waiter)

    await loop._loop()

    assert loop._loop_index == 0

    mock_daytime_waiter.wait.assert_called_once()
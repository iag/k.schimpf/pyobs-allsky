import asyncio
import datetime

import pytest

from pyobs_allsky.imaging_loop.daytime_waiter import DaytimeWaiter
from astropy.time import Time


def test_init(mocker, observer):
    time = datetime.datetime.strptime("2023-05-13T12:00:00", "%Y-%m-%dT%H:%M:%S")
    mocker.patch("astropy.time.Time.now", return_value=Time(time))
    waiter = DaytimeWaiter(observer)

    assert waiter._observer == observer
    assert waiter._sun_up

    time = datetime.datetime.strptime("2023-05-13T20:00:00", "%Y-%m-%dT%H:%M:%S")
    mocker.patch("astropy.time.Time.now", return_value=Time(time))
    waiter = DaytimeWaiter(observer)

    assert not waiter._sun_up


def test_get_sun_up(observer):
    waiter = DaytimeWaiter(observer)
    waiter._sun_up = True
    assert waiter._sun_up

    waiter._sun_up = False
    assert not waiter._sun_up


def test_update_sun_up(observer):
    waiter = DaytimeWaiter(observer)

    waiter._sun_up = True
    waiter._update_sun_up()
    assert not waiter._sun_up

    waiter._sun_up = False
    waiter._update_sun_up()
    assert waiter._sun_up


def test_get_sleep_time(observer):
    waiter = DaytimeWaiter(observer)
    assert 0 <= waiter._get_sleep_time() <= 12 * 3600


@pytest.mark.asyncio()
async def test_wait(mocker, observer):
    waiter = DaytimeWaiter(observer)
    mocker.patch.object(waiter, "_get_sleep_time", return_value=10)
    mocker.patch("asyncio.sleep")
    mocker.patch.object(waiter, "_update_sun_up")

    await waiter.wait()

    asyncio.sleep.assert_called_once_with(10)
    waiter._update_sun_up.assert_called_once()
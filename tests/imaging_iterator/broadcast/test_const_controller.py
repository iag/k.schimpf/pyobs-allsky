from pyobs_allsky.imaging_iterator.broadcast import ConstController


def test_init():
    controller = ConstController(True)
    assert controller._value

    controller = ConstController(False)
    assert not controller._value


def test_call():
    controller = ConstController(None)
    controller._value = True
    assert controller()

    controller._value = False
    assert not controller()

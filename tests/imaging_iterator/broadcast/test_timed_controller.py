import datetime

from pyobs_allsky.imaging_iterator.broadcast import TimedController


def test_init():
    controller = TimedController(120)
    assert controller._time_delta == 120
    assert controller._last_broad_casted == datetime.datetime.fromtimestamp(0)


def test_call(mocker):
    controller = TimedController(None)
    controller._time_delta = 120

    assert controller()
    assert not controller()

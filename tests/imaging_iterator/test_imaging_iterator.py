import numpy.testing
import pytest

from pyobs_allsky.imaging_iterator.imaging_iterator import ImagingIterator


@pytest.fixture()
def iterator(mock_camera_controller, exposure_time_setting, gain_setting, target_brightness_calculator, image_median_calculator):
    return ImagingIterator(mock_camera_controller, exposure_time_setting, gain_setting, target_brightness_calculator, image_median_calculator)


def test_init(mock_camera_controller, exposure_time_setting, gain_setting, target_brightness_calculator, image_median_calculator):
    iterator = ImagingIterator(mock_camera_controller, exposure_time_setting, gain_setting, target_brightness_calculator, image_median_calculator)

    assert iterator._camera == mock_camera_controller
    assert iterator._exposure_time_calculator._target_brightness_calc == target_brightness_calculator
    assert iterator._exposure_time_calculator._exposure_time_setting == exposure_time_setting
    assert iterator._exposure_time_calculator._gain_setting == gain_setting

    assert iterator._brightness_estimator == image_median_calculator
    assert iterator._target_brightness_calculator == target_brightness_calculator

    assert iterator._current_brightness == 0.0
    assert iterator._image is None


def test_get_camera_settings(mocker, iterator):
    mocker.patch.object(iterator._exposure_time_calculator, "get_exposure_time", return_value=1.0)
    mocker.patch.object(iterator._exposure_time_calculator, "get_gain", return_value=1.0)

    assert iterator._get_camera_settings() == (1.0, 1.0)

    iterator._exposure_time_calculator.get_exposure_time.assert_called_once_with()
    iterator._exposure_time_calculator.get_gain.assert_called_once_with()


@pytest.mark.asyncio
async def test_take_image(mocker, iterator, mock_image):
    mocker.patch.object(iterator, "_get_camera_settings", return_value=(1.0, 1.0))
    mocker.patch.object(iterator._camera, "apply_camera_settings")
    mocker.patch.object(iterator._camera, "take_image", return_value=mock_image)

    await iterator._take_image(True)

    iterator._get_camera_settings.assert_called_once_with()
    iterator._camera.apply_camera_settings.assert_called_once_with(1.0, 1.0)
    iterator._camera.take_image.assert_called_once_with(True)

    numpy.testing.assert_array_equal(mock_image, iterator._image)


@pytest.mark.asyncio
async def test_run_correction(mocker, iterator, mock_image):
    mocker.patch.object(iterator._brightness_estimator, "calc", return_value=100.0)
    mocker.patch.object(iterator._exposure_time_calculator, "calc")
    mocker.patch.object(iterator, "_take_image")

    iterator._image = mock_image

    await iterator._run_correction(True)

    iterator._brightness_estimator.calc.assert_called_once()
    iterator._exposure_time_calculator.calc.assert_called_once_with(100.0)
    iterator._take_image.assert_called_once_with(True)


@pytest.mark.asyncio
async def test_setup(mocker, iterator):
    mocker.patch.object(iterator, "_take_image")
    mocker.patch.object(iterator._target_brightness_calculator, "is_brightness_acceptable", return_value=True)

    await iterator.setup()

    iterator._take_image.assert_called_once()
    iterator._target_brightness_calculator.is_brightness_acceptable.assert_called_once()


@pytest.mark.asyncio
async def test_setup(iterator):
    setup_iterator = iterator.__aiter__()
    assert setup_iterator == iterator


@pytest.mark.asyncio
async def test_is_maxed_out(iterator):
    assert bool(iterator._is_maxed_out()) is False


@pytest.mark.asyncio
async def test_is_maxed_out_lower_end(iterator):
    iterator._current_brightness = 40000
    iterator._exposure_time_calculator._exposure_time_setting.set_value(64 * 1e-6)
    iterator._exposure_time_calculator._gain_setting.set_value(1.0)

    assert bool(iterator._is_maxed_out()) is True


@pytest.mark.asyncio
async def test_is_maxed_out_upper_end(iterator):
    iterator._current_brightness = 0.0
    iterator._exposure_time_calculator._exposure_time_setting.set_value(30.0)
    iterator._exposure_time_calculator._gain_setting.set_value(400.0)

    assert bool(iterator._is_maxed_out()) is True


@pytest.mark.asyncio
async def test_next(mocker, iterator, mock_image):
    mocker.patch.object(iterator, "_run_correction")
    iterator._image = mock_image
    image = await iterator.__anext__()

    iterator._run_correction.assert_called_once()

    numpy.testing.assert_array_equal(image.data, mock_image.data)


@pytest.mark.asyncio
async def test_loop(mocker, iterator, mock_image):
    mocker.patch.object(iterator, "_take_image")
    mocker.patch.object(iterator._target_brightness_calculator, "is_brightness_acceptable", return_value=True)
    mocker.patch.object(iterator, "_run_correction")
    iterator._image = mock_image

    async for image in iterator:
        break

    iterator._run_correction.assert_called_once()

    numpy.testing.assert_array_equal(image.data, mock_image.data)
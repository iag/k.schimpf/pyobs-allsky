import numpy.testing
import pytest
from pyobs.utils.enums import ImageFormat

from pyobs_allsky.imaging_iterator.broadcast import ConstController
from pyobs_allsky.imaging_iterator.camera_controller import CameraController


def test_init(mock_camera, mock_vfs):
    controller = CameraController(mock_camera, mock_vfs, True)
    assert controller._camera == mock_camera
    assert controller._vfs == mock_vfs
    assert controller._broadcast


@pytest.mark.asyncio
async def test_apply_camera_settings(mock_camera, mock_vfs):
    controller = CameraController(mock_camera, mock_vfs, True)
    await controller.apply_camera_settings(1.0, 1.0)

    mock_camera.set_exposure_time.assert_called_once_with(1.0)
    mock_camera.set_gain.assert_called_once_with(1.0)
    mock_camera.set_image_format.assert_called_once_with(ImageFormat.INT16)


@pytest.mark.asyncio
async def test_take_image(mock_camera, mock_vfs, mock_image):
    controller = CameraController(mock_camera, mock_vfs, ConstController(True))
    image = await controller.take_image(True)

    mock_camera.grab_data.assert_called_once_with(broadcast=True)
    mock_vfs.read_image.assert_called_once_with("serious_image_name.fits")

    numpy.testing.assert_array_equal(mock_image, image)

    controller = CameraController(mock_camera, mock_vfs, ConstController(False))
    image = await controller.take_image(True)

    mock_camera.grab_data.assert_called_with(broadcast=False)
    mock_vfs.read_image.assert_called_with("serious_image_name.fits")

    numpy.testing.assert_array_equal(mock_image, image)

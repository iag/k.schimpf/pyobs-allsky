import pytest

from pyobs_allsky.imaging_iterator.asi_auto_imaging_iterator import AsiAutoImagingIterator


def test_init(mock_camera_controller):
    loop = AsiAutoImagingIterator(mock_camera_controller, 1.0, 1.0)
    assert loop._camera == mock_camera_controller
    assert loop._exposure_time == 1.0
    assert loop._gain == 1.0


@pytest.mark.asyncio
async def test_setup(mocker, mock_camera_controller):
    mocker.patch.object(mock_camera_controller, "apply_camera_settings")
    loop = AsiAutoImagingIterator(mock_camera_controller, 1.0, 1.0)
    await loop.setup()

    assert mock_camera_controller.apply_camera_settings.call_count == 2
    mock_camera_controller.apply_camera_settings.assert_called_with(-1, -1)


@pytest.mark.asyncio
async def test_run(mocker, mock_camera_controller):
    mocker.patch.object(mock_camera_controller, "take_image")
    loop = AsiAutoImagingIterator(mock_camera_controller, 1.0, 1.0)
    await loop.next()

    mock_camera_controller.take_image.assert_called_once_with()

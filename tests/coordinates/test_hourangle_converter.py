import datetime

import pytest

import astropy.units as u
from astroplan import Observer

from pyobs_allsky.coordinates.hourangle_converter import HourAngleConverter


@pytest.fixture()
def mock_observer():
    return Observer(latitude=51.559299*u.deg, longitude=9.945472*u.deg, elevation=201*u.m)


def test_init(mock_observer):
    converter = HourAngleConverter(mock_observer)
    assert converter._observer == mock_observer


def test_call_reversible(mock_observer):
    converter = HourAngleConverter(mock_observer)
    time = datetime.datetime.now()

    ra = 0.0
    ha = converter.__call__(ra, time)
    conv_ra = converter.__call__(ha, time)

    assert ra == conv_ra

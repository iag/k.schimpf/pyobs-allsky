import datetime
import astropy.units as u
import numpy as np
import numpy.testing
from astropy.coordinates import SkyCoord

from pyobs_allsky.coordinates.altaz_pixel_converter import AltAzPixelConverter


def test_from_wcs(mock_image_header):
    converter = AltAzPixelConverter.from_image(mock_image_header)

    np.testing.assert_almost_equal(converter._hour_angle_converter._observer.location.lon.value,
                                   mock_image_header.header["LONGITUD"])
    np.testing.assert_almost_equal(converter._hour_angle_converter._observer.location.lat.value,
                                   mock_image_header.header["LATITUDE"])
    np.testing.assert_almost_equal(converter._hour_angle_converter._observer.location.height.value,
                                   mock_image_header.header["HEIGHT"])


def test_altaz_to_pixel(mock_image_header):
    converter = AltAzPixelConverter.from_image(mock_image_header)
    coord = SkyCoord(az=0*u.deg, alt=90*u.deg, frame="altaz", obstime=datetime.datetime.now())

    px, py = converter.alt_az_to_pixel(coord)

    assert px - 1.57558297e+03 < 100
    assert py - 1.01223730e+03 < 100

from pyobs_allsky.image.save import ImageSaver, JPGStrategy
from pyobs_allsky.image.save.image_saver_factory import ImageSaverFactory
from pyobs_allsky.image.save.pipeline import PipelineRunner
from pyobs_allsky.imaging_iterator.broadcast import TimedController, ConstController


def test_init():
    factory = ImageSaverFactory()
    assert factory._image_saver_config is None
    assert factory._image_saver is None


def test_set_image_saver_config():
    config = {"test": "test"}
    factory = ImageSaverFactory()
    factory.set_image_saver_config(config)

    assert factory._image_saver_config == config


def test_build_classes():
    config = {
        "class": "pyobs_allsky.image.save.ImageSaver",
        "file_path": "data/jpg/%Y%m%d%H%M%S.jpg",
        "saving_strategy": {
            "class": "pyobs_allsky.image.save.JPGStrategy",
        },
        "pipeline": [
            {"class": "pyobs_allsky.image.save.pipeline.DeBayerer"},
            {
                "class": "pyobs_allsky.image.save.pipeline.HeaderWriter",
                "header_keys": ["DATE-OBS"]
             }
        ],
        "time_btw_saves": 600
    }

    factory = ImageSaverFactory()
    factory._image_saver_config = config

    factory.build()

    assert factory._image_saver._file_path_creator._path_format == "data/jpg/%Y%m%d%H%M%S.jpg"
    assert isinstance(factory._image_saver._saving_strategy, JPGStrategy)
    assert isinstance(factory._image_saver._pipeline, PipelineRunner)
    assert isinstance(factory._image_saver._timer, TimedController)


def test_build_time_btw_saves():
    config = {
        "class": "pyobs_allsky.image.save.ImageSaver",
        "file_path": "data/jpg/%Y%m%d%H%M%S.jpg",
        "saving_strategy": {
            "class": "pyobs_allsky.image.save.JPGStrategy",
        },
        "pipeline": [
            {"class": "pyobs_allsky.image.save.pipeline.DeBayerer"},
            {
                "class": "pyobs_allsky.image.save.pipeline.HeaderWriter",
                "header_keys": ["DATE-OBS"]
            }
        ],
        "time_btw_saves": 600
    }

    factory = ImageSaverFactory()
    factory._image_saver_config = config

    factory.build()

    assert factory._image_saver._timer._time_delta == 600


def test_build_time_btw_saves_default():
    config = {
        "class": "pyobs_allsky.image.save.ImageSaver",
        "file_path": "data/jpg/%Y%m%d%H%M%S.jpg",
        "saving_strategy": {
            "class": "pyobs_allsky.image.save.JPGStrategy",
        },
        "pipeline": [
            {"class": "pyobs_allsky.image.save.pipeline.DeBayerer"},
            {
                "class": "pyobs_allsky.image.save.pipeline.HeaderWriter",
                "header_keys": ["DATE-OBS"]
            }
        ]
    }

    factory = ImageSaverFactory()
    factory._image_saver_config = config

    factory.build()

    assert isinstance(factory._image_saver._timer, ConstController)
    assert factory._image_saver._timer._value


def test_get_image_saver():
    factory = ImageSaverFactory()
    saver = ImageSaver("", {}, PipelineRunner([]), ConstController(True))
    factory._image_saver = saver

    assert factory.get_image_saver() == saver

import builtins

from pyobs_allsky.image.save.jpg.jpg_saver import JPGSaver


def test_init():
    saver = JPGSaver()
    assert saver._image is None


def test_set_image(mock_exif_image):
    saver = JPGSaver()
    saver.set_image(mock_exif_image)

    assert saver._image == mock_exif_image


def test_save(mocker, mock_exif_image):
    saver = JPGSaver()

    mocker.patch.object(mock_exif_image, "get_file")

    saver.set_image(mock_exif_image)

    mocker.patch('builtins.open')

    saver.save("test.jpg")

    builtins.open.assert_called_once_with("test.jpg", "wb")
    mock_exif_image.get_file.assert_called_once()
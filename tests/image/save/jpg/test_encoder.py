import exif

from pyobs_allsky.image.save.jpg.encoder import Encoder
from pyobs_allsky.image.save.jpg.meta_generator import MetaGenerator


def test_init():
    compressor = Encoder()
    assert compressor._image is None
    assert compressor._meta_generator is None


def test_set_image(mock_image):
    compressor = Encoder()
    compressor.set_image(mock_image)
    assert compressor._image == mock_image
    assert compressor._meta_generator._header == mock_image.header


def test_get_image(mock_image):
    compressor = Encoder()
    compressor._image = mock_image

    assert compressor.get_image() == mock_image
    assert compressor._image is None
    assert compressor._meta_generator is None


def test_normalize(mock_image):
    compressor = Encoder()
    mock_image.data[0, 0] = 500
    compressor._image = mock_image

    compressor._normalize()

    assert compressor._image.data[0, 0] == 255


def test_convert_to_exif(mock_image):
    compressor = Encoder()
    compressor._image = mock_image

    compressor._convert_to_exif()

    assert isinstance(compressor._image, exif.Image)


def test_write_meta_to_image(mock_exif_image):
    compressor = Encoder()
    compressor._image = mock_exif_image

    meta = {"artist": "IAG"}

    compressor._write_meta_to_image(meta)

    assert compressor._image.get("artist") == "IAG"


def test_compress(mock_image_header):
    compressor = Encoder()
    compressor._image = mock_image_header
    compressor._meta_generator = MetaGenerator(mock_image_header)

    compressor.encode()

    assert compressor._image.get("artist") == "IAG"

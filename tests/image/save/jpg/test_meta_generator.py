from pyobs_allsky.image.save.jpg.meta_generator import MetaGenerator


def test_init(mock_image_header):
    generator = MetaGenerator(mock_image_header)

    assert generator._header == mock_image_header.header


def test_get_datetime_str(mock_image_header):
    generator = MetaGenerator(mock_image_header)
    assert generator._get_datetime_str() == "2023:05:13 19:25:26"


def test_call(mocker, mock_image_header):
    generator = MetaGenerator(mock_image_header)
    mocker.patch.object(generator, "_get_datetime_str", return_value="2023:05:13 19:25:26")

    assert generator() == {
            "artist": "IAG",
            "exposure_time": 0.18215640,
            "datetime": "2023:05:13 19:25:26"
        }
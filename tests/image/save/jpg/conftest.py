import exif
import numpy as np
import pytest


@pytest.fixture()
def mock_exif_image():
    data = np.ones((4, 4))
    return exif.Image(data.tobytes())
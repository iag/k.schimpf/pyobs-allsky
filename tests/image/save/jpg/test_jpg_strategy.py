from pyobs_allsky.image.save import JPGStrategy
from pyobs_allsky.image.save.jpg.encoder import Encoder


def test_init():
    strategy = JPGStrategy()

    assert isinstance(strategy._encoder, Encoder)


def test_compress(mocker, mock_image, mock_exif_image):
    strategy = JPGStrategy()
    mocker.patch.object(strategy._encoder, "set_image")
    mocker.patch.object(strategy._encoder, "encode")
    mocker.patch.object(strategy._encoder, "get_image", return_value=mock_exif_image)

    assert strategy._encode(mock_image) == mock_exif_image

    strategy._encoder.set_image.assert_called_once_with(mock_image)
    strategy._encoder.encode.assert_called_once()
    strategy._encoder.get_image.assert_called_once()


def test_save(mocker, mock_exif_image):
    strategy = JPGStrategy()
    mocker.patch.object(strategy._saver, "set_image")
    mocker.patch.object(strategy._saver, "save")

    strategy._save(mock_exif_image, "test_path")

    strategy._saver.set_image.assert_called_once_with(mock_exif_image)
    strategy._saver.save.assert_called_once_with("test_path")

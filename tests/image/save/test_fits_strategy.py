from pyobs_allsky.image.save.fits_strategy import FitsStrategy


def test_init():
    saver = FitsStrategy(overwrite=True)

    assert saver._overwrite


def test_save(mocker, mock_image):
    saver = FitsStrategy(overwrite=False)
    mocker.patch.object(mock_image, "writeto")

    saver.save(mock_image, "data/test.fits")

    mock_image.writeto.assert_called_once_with("data/test.fits", overwrite=False)

    saver._overwrite = True
    saver.save(mock_image, "data/test.fits")
    mock_image.writeto.assert_called_with("data/test.fits", overwrite=True)

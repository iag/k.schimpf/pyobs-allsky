import pytest
from pyobs.images import Image

from pyobs_allsky.image.save.pipeline.processor.processor import Processor
from pyobs_allsky.image.save.pipeline.pipeline_runner import PipelineRunner


class MockProcessor(Processor):
    def process(self, image: Image) -> Image:
        if "COUNTER" in image.header.keys():
            image.header["COUNTER"] = image.header["COUNTER"] + 1
        else:
            image.header["COUNTER"] = 0
        return image


def test_init():
    processes = [MockProcessor(), MockProcessor(), MockProcessor()]
    runner = PipelineRunner(processes)
    assert runner._pipeline == processes


def test_run_mock(mock_image_header):
    processes = [MockProcessor(), MockProcessor(), MockProcessor()]
    runner = PipelineRunner(processes)
    image = runner.run(mock_image_header)
    assert image != mock_image_header
    assert image.header["COUNTER"] == 2

import numpy as np
import numpy.testing
from matplotlib import pyplot as plt

from pyobs_allsky.image.save.pipeline import AltAzCoordinatePlotter


def test_init():
    plotter = AltAzCoordinatePlotter(lod=0.5)

    assert plotter._datetime is not None
    assert plotter._converter is None
    assert plotter._image is None
    assert plotter._canvas is None

    assert plotter._lod == 0.5


def test_pixel_coordinate_in_image(mock_image_header):
    plotter = AltAzCoordinatePlotter()
    plotter._image = mock_image_header

    assert plotter._pixel_coordinate_in_image(0, 0)

    assert not plotter._pixel_coordinate_in_image(-1, 0)
    assert not plotter._pixel_coordinate_in_image(0, -1)

    assert not plotter._pixel_coordinate_in_image(5, 0)
    assert not plotter._pixel_coordinate_in_image(0, 5)


def test_filter_pixel_coordinates(mock_image_header):
    plotter = AltAzCoordinatePlotter()
    plotter._image = mock_image_header

    coords = [(0, 0), (1, 1), (-1, 0), (0, -1), (5, 0), (0, 5), (3, 3)]

    np.testing.assert_array_equal(plotter._filter_pixel_coordinates(coords), [(0, 0+plotter._Y_PAD), (1, 1+plotter._Y_PAD), (3, 3+plotter._Y_PAD)])


def test_process(mock_image_header_large):
    plotter = AltAzCoordinatePlotter(lod=0.1)
    mock_image_header_large.data[0, 0] = [2**16-1, 2**16-1, 2**16-1]
    img = plotter.process(mock_image_header_large)
    #plt.imshow(img.data)
    #plt.show()

    np.testing.assert_array_equal(img.data[np.shape(mock_image_header_large.data)[0] - 1 + 100, 0], [2**16-1, 2**16-1, 2**16-1])

    assert plotter._converter is not None
    assert plotter._image is not None
    assert plotter._canvas is not None

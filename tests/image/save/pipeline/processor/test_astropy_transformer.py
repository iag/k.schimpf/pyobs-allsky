import astropy.visualization
import numpy as np
import numpy.testing

from pyobs_allsky.image.save.pipeline import AstropyTransformer


def test_init():
    config = {"class": "astropy.visualization.PercentileInterval", "percentile": 99.9}
    transform = AstropyTransformer(config)

    assert isinstance(transform._transformer, astropy.visualization.PercentileInterval)


def test_process(mock_image):
    config = {"class": "astropy.visualization.LinearStretch", "slope": 2.0, "intercept": 0.0}
    transform = AstropyTransformer(config)

    image = transform.process(mock_image)
    numpy.testing.assert_equal(image.data, np.ones((4, 4)) * 2.0)

import cv2
import numpy as np
from pyobs.images import Image

from pyobs_allsky.image.save.pipeline import HeaderWriter


def test_init(mock_image):
    keys = ["test"]
    processor = HeaderWriter(keys)

    assert processor._header_keys == keys


def test_gen_header_text(mock_image_header):
    processor = HeaderWriter(["DATE-OBS", "EXPTIME"])
    processor._image = mock_image_header

    assert processor._gen_header_text(mock_image_header) == ["13.05.2023 19:25:26", "EXPTIME: 0.18"]


def test_write_header_on_image(mocker, mock_image):
    processor = HeaderWriter(["DATE-OBS"])
    mock_image.data = np.ones((100, 100, 3))

    mocker.patch("cv2.putText", return_value=mock_image)

    image = processor._write_header_on_image(["header text"], mock_image)

    cv2.putText.assert_called()
    assert image == mock_image


def test_process(mocker, mock_image_header):
    processor = HeaderWriter(["DATE-OBS"])
    mocker.spy(processor, "_gen_date")
    mocker.spy(processor, "_gen_header_text")
    mocker.spy(processor, "_write_header_on_image")

    assert isinstance(processor.process(mock_image_header), Image)

    processor._gen_date.assert_called_once()
    processor._gen_header_text.assert_called_once()
    processor._write_header_on_image.assert_called_once()

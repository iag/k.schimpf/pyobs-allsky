import numpy as np
from pyobs.images import Image

from pyobs_allsky.image.save.pipeline import DeBayerer


def test_de_bayer(mock_image):
    processor = DeBayerer()
    result_image = processor._de_bayer(mock_image)

    assert np.shape(result_image.data) == (4, 4, 3)


def test_normalize(mock_image):
    processor = DeBayerer()
    mock_image.data[0, 0] = 2**17 - 1

    result_image = processor._normalize(mock_image)

    assert result_image.data[0, 0] == 2**16 - 1


def test_process(mocker, mock_image):
    processor = DeBayerer()
    mocker.spy(processor, "_de_bayer")
    mocker.spy(processor, "_normalize")

    assert isinstance(processor.process(mock_image), Image)

    processor._de_bayer.assert_called_once()
    processor._normalize.assert_called_once()
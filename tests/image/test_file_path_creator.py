import os
from pyobs_allsky.image.file_path_creator import FilePathCreator


def test_init():
    creator = FilePathCreator("/test/%Y%m%d/%Y%m%d%H%M%S.fits")

    assert creator._path_format == "/test/%Y%m%d/%Y%m%d%H%M%S.fits"
    assert creator._path is None


def test_get():
    creator = FilePathCreator("/test/%Y%m%d/%Y%m%d%H%M%S.fits")
    creator._path = "/test/20230628/010101.fits"
    assert creator.get_path() == "/test/20230628/010101.fits"


def test_update(mock_image_header):
    creator = FilePathCreator("/test/%Y%m%d/%Y%m%d%H%M%S.fits")
    creator.update(mock_image_header)

    assert creator._path == "/test/20230513/20230513192526.fits"


def test_call(mocker):
    mocker.patch("os.makedirs")

    creator = FilePathCreator("/test/%Y%m%d")
    creator._path = "/test/20230513/20230513192526.fits"
    creator.create()

    os.makedirs.assert_called_once_with("/test/20230513", exist_ok=True)

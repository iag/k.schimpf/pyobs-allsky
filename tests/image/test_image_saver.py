import pytest

from pyobs_allsky.image.file_path_creator import FilePathCreator
from pyobs_allsky.image.save import FitsStrategy
from pyobs_allsky.image.save.image_saver import ImageSaver
from pyobs_allsky.imaging_iterator.broadcast import ConstController


@pytest.fixture()
def mock_file_path_creator(mocker):
    creator = FilePathCreator("test/%Y%m%d/%Y%m%d%H%M%S.jpg")
    mocker.patch.object(creator, "update")
    mocker.patch.object(creator, "create", return_value=1)
    mocker.patch.object(creator, "get_path", return_value="test/20230628/010101.jpg")

    return creator


@pytest.fixture()
def mock_saving_strategy(mocker):
    saving_strategy = FitsStrategy()
    mocker.patch.object(saving_strategy, "save")

    return saving_strategy


def test_init(mock_file_path_creator, mock_saving_strategy, mock_pipeline):
    timer = ConstController(True)
    image_saver = ImageSaver(mock_file_path_creator, mock_saving_strategy, mock_pipeline, timer)

    assert image_saver._file_path_creator == mock_file_path_creator
    assert image_saver._saving_strategy == mock_saving_strategy
    assert image_saver._pipeline == mock_pipeline
    assert image_saver._timer == timer


def test_generate_file_path(mock_file_path_creator, mock_saving_strategy, mock_image, mock_pipeline):
    image_saver = ImageSaver(mock_file_path_creator, mock_saving_strategy, mock_pipeline, ConstController(True))

    assert image_saver._generate_file_path(mock_image) == "test/20230628/010101.jpg"

    mock_file_path_creator.update.assert_called_once_with(mock_image)
    mock_file_path_creator.create.assert_called_once()
    mock_file_path_creator.get_path.assert_called_once()


def test_save_called(mocker, mock_file_path_creator, mock_saving_strategy, mock_image, mock_pipeline):
    image_saver = ImageSaver(mock_file_path_creator, mock_saving_strategy, mock_pipeline, ConstController(True))

    mocker.patch.object(image_saver, "_generate_file_path", return_value="test/20230628/latest.jpg")

    image_saver.save(mock_image)

    mock_pipeline.run.assert_called_once_with(mock_image)
    image_saver._generate_file_path.assert_called_once()
    mock_saving_strategy.save.assert_called_once()


def test_save_not_called(mocker, mock_file_path_creator, mock_saving_strategy, mock_image, mock_pipeline):
    image_saver = ImageSaver(mock_file_path_creator, mock_saving_strategy, mock_pipeline, ConstController(False))

    mocker.patch.object(image_saver, "_generate_file_path", return_value="test/20230628/latest.jpg")

    image_saver.save(mock_image)

    assert not mock_pipeline.run.called
    assert not image_saver._generate_file_path.called
    assert not mock_saving_strategy.save.called
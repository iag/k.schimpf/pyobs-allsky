import asyncio
from typing import Any, List

import numpy as np
import pytest
from astroplan import Observer
from pyobs.images import Image
from pyobs.interfaces import IImageFormat, IGain
from pyobs.modules.camera import BaseCamera
from pyobs.utils.enums import ImageFormat
from pyobs.vfs import VirtualFileSystem

import astropy.units as u

from pyobs_allsky.camera_settings.converted_camera_settings import ConvertedIntegerCameraSetting
from pyobs_allsky.image.file_path_creator import FilePathCreator
from pyobs_allsky.imaging_iterator.broadcast import ConstController
from tests.image.save.pipeline.test_pipeline_runner import MockProcessor
from pyobs_allsky.brightness_estimator.image_median_calculator import ImageMedianCalculator
from pyobs_allsky.camera_settings.camera_setting import CameraSetting
from pyobs_allsky.camera_settings.target_brightness_calculator import TargetBrightnessCalculator
from pyobs_allsky.image.save import ImageSaver, JPGStrategy, FitsStrategy
from pyobs_allsky.image.save.pipeline import PipelineRunner

from pyobs_allsky.imaging_iterator.asi_auto_imaging_iterator import AsiAutoImagingIterator
from pyobs_allsky.imaging_iterator.camera_controller import CameraController


@pytest.fixture()
def mask():
    zeros = np.zeros((2, 2))
    ones = np.ones((2, 2))

    row = np.concatenate((zeros, ones), axis=0)
    return np.concatenate((row, row), axis=1)


@pytest.fixture()
def target_brightness_calculator():
    return TargetBrightnessCalculator(8000, 0.1, 1.1)


@pytest.fixture()
def exposure_time_setting():
    return CameraSetting(1.0, 0.00064, 30.0)


@pytest.fixture()
def gain_setting():
    return ConvertedIntegerCameraSetting(1.0, 1.0, 400.0)


@pytest.fixture()
def image_median_calculator(mask):
    return ImageMedianCalculator(mask)


@pytest.fixture()
def image_iterator(mock_camera_controller):
    return AsiAutoImagingIterator(mock_camera_controller, 1.0, 1.0)


@pytest.fixture()
def mock_pipeline(mocker):
    runner = PipelineRunner([MockProcessor()])
    mocker.patch.object(runner, "run")
    return runner


@pytest.fixture()
def fits_saver(mock_pipeline):
    save_strategy = FitsStrategy()
    file_path_creator = FilePathCreator("data/latest.fits")
    return ImageSaver(file_path_creator, save_strategy, mock_pipeline, ConstController(True))


@pytest.fixture()
def compressed_saver(mock_pipeline):
    save_strategy = JPGStrategy()
    file_path_creator = FilePathCreator("data/latest.fits")

    return ImageSaver(file_path_creator, save_strategy, mock_pipeline, ConstController(True))


class MockCamera(BaseCamera, IGain, IImageFormat):
    async def _expose(self, exposure_time: float, open_shutter: bool, abort_event: asyncio.Event) -> Image:
        pass

    async def get_image_format(self, **kwargs: Any) -> ImageFormat:
        pass

    async def list_image_formats(self, **kwargs: Any) -> List[str]:
        pass

    async def set_image_format(self, fmt: ImageFormat, **kwargs: Any) -> None:
        pass

    async def set_gain(self, gain: float, **kwargs: Any) -> None:
        pass

    async def get_gain(self, **kwargs: Any) -> float:
        pass


@pytest.fixture()
def mock_camera(mocker):
    camera = MockCamera()

    mocker.patch.object(camera, "grab_data", return_value="serious_image_name.fits")
    mocker.patch.object(camera, "set_exposure_time")
    mocker.patch.object(camera, "set_gain")
    mocker.patch.object(camera, "set_image_format")

    return camera


@pytest.fixture()
def mock_image():
    data = np.ones((4, 4))
    image = Image(data)
    return image


@pytest.fixture()
def mock_image_header(mock_image):
    mock_image.header["DATE-OBS"] = "2023-05-13T19:25:26.536781"
    mock_image.header["EXPTIME"] = 0.18215640

    mock_image.header["LATITUDE"] = 51.559299
    mock_image.header["LONGITUD"] = 9.945472
    mock_image.header["HEIGHT"] = 201

    mock_image.header["WCSAXES"] = 2
    mock_image.header["CRPIX1"] = 1195.2545605796
    mock_image.header["CRPIX2"] = 1031.8076958883
    mock_image.header["PC1_1"] = -0.00060084027941286
    mock_image.header["PC1_2"] = -0.052057192086615
    mock_image.header["PC2_1"] = 0.054891854602827
    mock_image.header["PC2_2"] = -0.00073949896926166
    mock_image.header["CDELT1"] = 1.0
    mock_image.header["CDELT2"] = 1.0
    mock_image.header["CUNIT1"] = "deg"
    mock_image.header["CUNIT2"] = "deg"
    mock_image.header["CTYPE1"] = "RA---AIR-SIP"
    mock_image.header["CTYPE2"] = "DEC--AIR-SIP"
    mock_image.header["CRVAL1"] = 359.61108550652
    mock_image.header["CRVAL2"] = 31.416129776264
    mock_image.header["LONPOLE"] = 180.0
    mock_image.header["LATPOLE"] = 31.416129776264
    mock_image.header["MJDREF"] = 0.0
    mock_image.header["RADESYS"] = "ICRS"
    
    mock_image.header["A_ORDER"] = 3
    mock_image.header["A_0_2"] = -0.00015630563285242156
    mock_image.header["A_0_3"] = -1.6225786855943367e-09
    mock_image.header["A_1_1"] = -7.11405885212094e-06
    mock_image.header["A_1_2"] = 6.641229215631077e-08
    mock_image.header["A_2_0"] = -8.509167837901942e-05
    mock_image.header["A_2_1"] = -2.858253312610882e-09
    mock_image.header["A_3_0"] = 9.940844793780548e-08
    mock_image.header["B_ORDER"] = 3
    mock_image.header["B_0_2"] = 4.976793473695487e-06
    mock_image.header["B_0_3"] = 8.709951903395686e-08
    mock_image.header["B_1_1"] = 7.54764949893915e-05
    mock_image.header["B_1_2"] = -1.0402281194607352e-09
    mock_image.header["B_2_0"] = 7.503354619152504e-06
    mock_image.header["B_2_1"] = 1.282581599768554e-07
    mock_image.header["B_3_0"] = 2.44448533926602e-09
    mock_image.header["AP_ORDER"] = 3
    mock_image.header["BP_ORDER"] = 3

    mock_image.header["NAXIS1"], mock_image.header["NAXIS2"] = 2821,  2071

    return mock_image


@pytest.fixture()
def mock_image_header_large(mock_image_header):
    mock_image_header.data = np.zeros((2080, 3096, 3))
    return mock_image_header


@pytest.fixture()
def mock_vfs(mocker, mock_image):
    vfs = VirtualFileSystem()

    mocker.patch.object(vfs, "read_image", return_value=mock_image)

    return vfs


@pytest.fixture()
def mock_camera_controller(mock_camera, mock_vfs):
    return CameraController(mock_camera, mock_vfs, False)


@pytest.fixture()
def observer():
    return Observer(latitude=51.559299 * u.deg, longitude=51.559299 * u.deg, elevation=201 * u.meter)

import numpy as np
import numpy.testing

from pyobs_allsky.brightness_estimator.image_average_calculator import ImageAverageCalculator


def test_init_default(mask):
    calculator = ImageAverageCalculator(mask)
    np.testing.assert_array_equal(calculator._mask, mask)

    assert calculator._offset == 0.0
    assert calculator._scaling_factor == 1.0


def test_init(mask):
    calculator = ImageAverageCalculator(mask, 34.0, 4)
    np.testing.assert_array_equal(calculator._mask, mask)

    assert calculator._offset == 34.0
    assert calculator._scaling_factor == 4


def test_calc_default(mask):
    calculator = ImageAverageCalculator(mask)
    image = np.reshape(np.arange(0, 16), (4, 4))
    assert calculator.calc(image) == 11.5


def test_calc_offset(mask):
    calculator = ImageAverageCalculator(mask, 1.0)
    image = np.reshape(np.arange(0, 16), (4, 4))
    assert calculator.calc(image) == 10.5


def test_calc_scaling(mask):
    calculator = ImageAverageCalculator(mask, scaling_factor=4)
    image = np.reshape(np.arange(0, 16), (4, 4))
    assert calculator.calc(image) == 11.5/4
from pyobs_allsky.camera_settings.target_brightness_calculator import TargetBrightnessCalculator


def test_init():
    calculator = TargetBrightnessCalculator(8000, 0.1, 1.1)
    assert calculator._aggressiveness == 1.1
    assert calculator._min_acceptable_brightness is not None
    assert calculator._max_acceptable_brightness is not None


def test_set_acceptable_brightness():
    calculator = TargetBrightnessCalculator(8000, 0.1, 1.1)
    calculator._set_acceptable_brightness(2000, 0.1)

    assert calculator._min_acceptable_brightness == 1800
    assert calculator._max_acceptable_brightness == 2200

    calculator._set_acceptable_brightness(2000, 2)

    assert calculator._min_acceptable_brightness == 0
    assert calculator._max_acceptable_brightness == 6000


def test_calc_target_brightness():
    calculator = TargetBrightnessCalculator(8000, 0.1, 1.1)

    assert calculator.calc(7000) == (8800, 1.1)
    assert calculator.calc(9000) == (7200, 1.0 / 1.1)


def test_is_brightness_acceptable():
    calculator = TargetBrightnessCalculator(8000, 0.1, 1.1)

    assert calculator.is_brightness_acceptable(8000)
    assert not calculator.is_brightness_acceptable(7000)
    assert not calculator.is_brightness_acceptable(9000)

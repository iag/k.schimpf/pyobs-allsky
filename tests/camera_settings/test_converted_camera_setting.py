from pyobs_allsky.camera_settings.converted_camera_settings import ConvertedIntegerCameraSetting


def test_set_converted():
    camera_setting = ConvertedIntegerCameraSetting(
        1.0, 1.0, 100.0
    )

    camera_setting.set_converted(1.5)
    assert camera_setting.get_value() == 2


def test_get_converted():
    camera_setting = ConvertedIntegerCameraSetting(
        1.0, 1.0, 100.0
    )

    camera_setting._camera_setting._value = 20.0
    assert camera_setting.get_converted() == 20.0


def test_get_max_converted():
    camera_setting = ConvertedIntegerCameraSetting(
        1.0, 1.0, 100.0
    )

    camera_setting._camera_setting._value = 20.0
    assert camera_setting.get_max_converted() == 100.0

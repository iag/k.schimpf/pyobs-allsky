from pyobs_allsky.camera_settings.calculator.calculator import Calculator


def test_init(target_brightness_calculator, exposure_time_setting, gain_setting):
    calculator = Calculator(target_brightness_calculator, exposure_time_setting, gain_setting)

    assert calculator._target_brightness_calc == target_brightness_calculator
    assert calculator._exposure_time_setting == exposure_time_setting
    assert calculator._gain_setting == gain_setting


def test_calc_accepted(mocker, target_brightness_calculator, exposure_time_setting, gain_setting):
    mocker.patch.object(target_brightness_calculator, "is_brightness_acceptable", return_value=True)
    mocker.patch.object(target_brightness_calculator, "calc", return_value=(100, 1.0))
    calculator = Calculator(target_brightness_calculator, exposure_time_setting, gain_setting)

    mocker.patch.object(calculator, "_calc_multiplier", return_value=(1.0, 1.0))
    mocker.patch.object(calculator, "_update_settings")

    calculator.calc(100.0)

    target_brightness_calculator.is_brightness_acceptable.assert_called_with(100.0)

    assert not calculator._target_brightness_calc.calc.called
    assert not calculator._calc_multiplier.called
    assert not calculator._update_settings.called


def test_calc(mocker, target_brightness_calculator, exposure_time_setting, gain_setting):
    mocker.patch.object(target_brightness_calculator, "is_brightness_acceptable", return_value=False)
    mocker.patch.object(target_brightness_calculator, "calc", return_value=(100, 1.0))
    calculator = Calculator(target_brightness_calculator, exposure_time_setting, gain_setting)

    mocker.patch.object(calculator, "_calc_multiplier", return_value=1.0)
    mocker.patch.object(calculator, "_update_settings")

    calculator.calc(100.0)

    target_brightness_calculator.is_brightness_acceptable.assert_called_with(100.0)

    calculator._target_brightness_calc.calc.assert_called_once_with(100.0)
    calculator._calc_multiplier.assert_called_once_with(100.0, 100.0, 1.0)
    calculator._update_settings.assert_called_once_with(1.0)


def test_update_settings_exp_time(mocker, target_brightness_calculator, exposure_time_setting, gain_setting):
    mocker.patch.object(exposure_time_setting, "get_max_value", return_value=30)
    mocker.patch.object(exposure_time_setting, "set_value")

    mocker.patch.object(gain_setting, "set_value")

    calculator = Calculator(target_brightness_calculator, exposure_time_setting, gain_setting)

    calculator._update_settings(20)

    exposure_time_setting.set_value.assert_called_once_with(20)
    gain_setting.set_value.assert_called_once_with(1)


def test_update_settings_gain(mocker, target_brightness_calculator, exposure_time_setting, gain_setting):
    mocker.patch.object(exposure_time_setting, "get_max_value", return_value=30)
    mocker.patch.object(exposure_time_setting, "set_value")

    mocker.patch.object(gain_setting, "set_converted")

    calculator = Calculator(target_brightness_calculator, exposure_time_setting, gain_setting)

    calculator._update_settings(90)

    exposure_time_setting.set_value.assert_called_once_with(90)
    gain_setting.set_converted.assert_called_once_with(3)


def test_get_exposure_time(mocker, target_brightness_calculator, exposure_time_setting, gain_setting):
    mocker.patch.object(exposure_time_setting, "get_value", return_value=10.0)
    calculator = Calculator(target_brightness_calculator, exposure_time_setting, gain_setting)

    assert calculator.get_exposure_time() == 10.0
    calculator._exposure_time_setting.get_value.assert_called_once_with()


def test_get_gain(mocker, target_brightness_calculator, exposure_time_setting, gain_setting):
    mocker.patch.object(gain_setting, "get_value", return_value=10.0)
    calculator = Calculator(target_brightness_calculator, exposure_time_setting, gain_setting)

    calculator._gain_setting.get_value.called
    assert calculator.get_gain() == 10.0

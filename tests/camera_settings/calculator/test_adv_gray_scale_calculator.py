from pyobs_allsky.camera_settings.calculator import AdvGrayScaleCalculator


def test_calc_multiplier(mocker, target_brightness_calculator, exposure_time_setting, gain_setting):
    calculator = AdvGrayScaleCalculator(target_brightness_calculator, exposure_time_setting, gain_setting)

    assert calculator._calc_multiplier(6000, 4000, 1.0) < 1.0
    assert calculator._calc_multiplier(2000, 4000, 1.0) > 1.0
    assert calculator._calc_multiplier(4000, 4000, 1.0) == 1.0

from pyobs_allsky.camera_settings.calculator import FalsePositiveCalculator

def test_calc_multiplier(mocker, target_brightness_calculator, exposure_time_setting, gain_setting):
    calculator = FalsePositiveCalculator(target_brightness_calculator, exposure_time_setting, gain_setting)

    assert calculator._calc_multiplier(500, 1000, 1.0) == 2.0
    assert calculator._calc_multiplier(1000, 500, 1.0) == 0.5
    assert calculator._calc_multiplier(500, 1000, 2.0) == 4.0


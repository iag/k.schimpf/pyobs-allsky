import pytest
from pyobs_allsky.camera_settings import CameraSetting


def test_valid_init():
    value = 10
    min_value = 1
    max_value = 20

    cam_value = CameraSetting(value, min_value, max_value)

    assert cam_value._value == value
    assert cam_value._min_value == min_value
    assert cam_value._max_value == max_value


def test_invalid_init():
    with pytest.raises(ValueError):
        CameraSetting(0, 10, 20)

    with pytest.raises(ValueError):
        CameraSetting(30, 20, 10)


def test_get_value():
    value = 10

    cam_value = CameraSetting(value, 1, 20)
    assert cam_value.get_value() == value


def test_apply_constraints():
    cam_value = CameraSetting(10, 1, 20)
    cam_value._apply_constraints()
    assert cam_value._value == 10

    cam_value._value = 0
    cam_value._apply_constraints()
    assert cam_value._value == 1

    cam_value._value = 21
    cam_value._apply_constraints()
    assert cam_value._value == 20


def test_set_value():
    cam_value = CameraSetting(10, 1, 20)

    cam_value.set_value(0)
    assert cam_value._value == 1

    cam_value.set_value(21)
    assert cam_value._value == 20


def test_get_max_ratio():
    cam_value = CameraSetting(10.0, 1.0, 20.0)
    assert cam_value.calc_max_ratio() == 2.0

    assert cam_value.calc_min_ratio() == 0.1


def test_is_value_max():
    cam_value = CameraSetting(10.0, 1.0, 20.0)
    assert not cam_value.is_value_max()

    cam_value._value = 1.0
    assert not cam_value.is_value_max()

    cam_value._value = 20.0
    assert cam_value.is_value_max()


def test_is_value_min():
    cam_value = CameraSetting(10.0, 1.0, 20.0)
    assert not cam_value.is_value_min()

    cam_value._value = 1.0
    assert cam_value.is_value_min()

    cam_value._value = 20.0
    assert not cam_value.is_value_min()

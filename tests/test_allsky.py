import pyobs_allsky
from pyobs_allsky import AllSky
from pyobs_allsky.imaging_loop import ImagingLoopFactory


def test_init(mocker):
    broadcast = True
    camera_name = "camera"
    time_btw_images = 30.0
    start_exposure_time = 1.0
    min_exposure_time = 0.00064
    max_exposure_time = 30.0
    start_gain = 1.0
    min_gain = 1.0
    max_gain = 400
    mask_filepath = "mask.npy"
    brightness_offset = 34
    brightness_scaling = 4
    acceptable_brightness = 8000
    acceptable_brightness_deviation = 0.1
    aggressiveness = 1.1
    image_saver = []

    spy = mocker.spy(ImagingLoopFactory, "__init__")

    allsky = AllSky(camera_name,
                    start_exposure_time,
                    min_exposure_time,
                    max_exposure_time,
                    start_gain,
                    min_gain,
                    max_gain,
                    mask_filepath,
                    brightness_offset,
                    brightness_scaling,
                    {}, {})

    spy.assert_called_once()
    assert allsky._imaging_loop_factory is not None
    assert allsky._camera_name == camera_name
    assert allsky._day_loop is None
    assert allsky._night_loop is None
    assert allsky._imaging_task is None